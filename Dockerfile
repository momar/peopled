FROM node:alpine AS ui

RUN apk add --no-cache yarn

COPY ui /app/ui
WORKDIR /app/ui
RUN yarn install && yarn build

FROM golang:alpine

RUN apk add --no-cache ca-certificates

COPY go.mod /app/
RUN cd /app && go mod download

COPY api /app/api
COPY cmd /app/cmd
COPY ldap /app/ldap
COPY resources /app/resources
RUN mv /app/cmd/peopled/config.go /app/cmd/peopled/config.go.example && ln -s /var/peopled/config.go /app/cmd/peopled/config.go

ENV UI_PATH /app/ui/dist
COPY --from=ui /app/ui/dist /app/ui/dist

WORKDIR /app/cmd/peopled
ENV TOKEN_DATABASE /var/peopled/token-database.bolt
ENV APPS_PATH /var/peopled/apps.json
ENV TABS_PATH /var/peopled/tabs.json
ENV PORT 80
EXPOSE 80
# TODO: only build if something changed!
CMD ["/bin/sh", "-c" , " \
  [ -d /var/peopled ] || mkdir /var/peopled; \
  [ -f /var/peopled/config.go ] || cp /app/cmd/peopled/config.go.example /var/peopled/config.go; \
  [ -f /var/peopled/tabs.json ] || cp /app/resources/tabs.json /var/peopled/tabs.json; \
  [ -f /var/peopled/apps.json ] || cp /app/resources/apps.json /var/peopled/apps.json; \
  /usr/local/go/bin/go mod tidy && \
  exec /usr/local/go/bin/go run . \
"]
