# Peopled - the flexible user portal

**This is currently under development, and not yet ready for production use!**

Peopled is an LDAP administration tool that fits *your* LDAP server. To make it easy to use, the Docker image provides a sane default configuration

To get started, just start the server using `docker-compose up -d` with the following `docker-compose.yml` file - make sure to replace your domain names before:
- `account.werkundkultur.de` is the external hostname where your LDAP server is running at and where your Peopled UI will be reachable
- `werkundkultur.de` is the internal LDAP domain where your users and groups will be stored - it will be treated as `dc=werkundkultur,dc=de` internally

```yaml
version: "3"
services:
  # Reverse proxy for TLS termination.
  proxy:
    image: caddy
    command: ["sh", "-c", "\
      if ! [ -f /etc/sudoers ]; then \
        adduser -u 389 -D caddy && \
        apk add --no-cache libcap sudo && \
        setcap CAP_NET_BIND_SERVICE=+eip /usr/bin/caddy; \
      fi; \
      chown -R caddy. /data/caddy && \
      exec sudo -Eu caddy caddy reverse-proxy --from \"$$DOMAIN\" --to peopled:80"]
    restart: unless-stopped
    ports: ["80:80", "443:443"]
    environment:
      # TODO: adjust the external domain
      DOMAIN: account.werkundkultur.de
    volumes:
    - "./data/caddy:/data/caddy"
  
  # OpenLDAP container with automated backups.
  openldap:
    image: tiredofit/openldap:7.1.7
    restart: unless-stopped
    ports:
    - "389:389"
    - "636:636"
    volumes:
    - "./data/ldap-data:/var/lib/openldap"
    - "./data/ldap-slapd.d:/etc/openldap/slapd.d"
    - "./data/ldap-backup:/data/backup"
    - "./data/ldap-certs:/assets/slapd/certs"
    # when providing your own certificates, make sure that they're readable by UID 389
    - "./data/caddy/certificates/acme-v02.api.letsencrypt.org-directory:/certs/caddy:ro"
    environment:
      ORGANIZATION: "Werk- und Kulturscheune" # TODO: give your organisation a name
      DOMAIN: "werkundkultur.de" # TODO: choose an LDAP domain
      TIMEZONE: Europe/Berlin # TODO: use the correct timezone, see https://en.wikipedia.org/wiki/List_of_tz_database_time_zones 
      
      # DEBUG_MODE: "true" # Uncomment this for debugging
      
      # TODO: generate one password for each user: echo $(cat /dev/urandom | tr -dc 'A-Za-z0-9' | head -c64 | tail)
      ADMIN_PASS: ""
      CONFIG_PASS: ""
      READONLY_USER_PASS: ""
      ENABLE_READONLY_USER: "true"
      
      # TODO: domain must be the same as in Caddy
      # when using your own certificates, make sure to use the full chain
      TLS_CRT_FILENAME: caddy/account.werkundkultur.de/account.werkundkultur.de.crt
      TLS_CA_CRT_FILENAME: caddy/account.werkundkultur.de/account.werkundkultur.de.crt
      TLS_KEY_FILENAME: caddy/account.werkundkultur.de/account.werkundkultur.de.key
      TLS_CA_CRT_PATH: /certs/
      TLS_RESET_PERMISSIONS: "false"
      
      TLS_VERIFY_CLIENT: never
      TLS_ENFORCE: "true"
      ENABLE_NGINX: "false"

  # Container for the Peopled administration UI.
  peopled:
    image: momar/peopled
    restart: unless-stopped
    volumes:
    - "./data/peopled:/var/peopled"
    # ports: ["80:80"] # peopled doesn't support TLS by itself, you will need a reverse proxy (like Caddy; see above)
    environment:
      # TODO: domain must be the same as in Caddy, but with a "https://" prefix - for embedding, see below
      CANONICAL_ROOT_URL: "https://account.werkundkultur.de"
      TRUST_PROXY: "true"
      # LOG_LEVEL: trace # uncomment to enable debugging
      
      LDAP_ADDRESS: ldaps://account.werkundkultur.de:636 # TODO: must be the same as in Caddy
      LDAP_DOMAIN: "werkundkultur.de" # TODO: must be the same as LDAP_DOMAIN above
      LDAP_PASSWORD: "" # TODO: must be the same as LDAP_ADMIN_PASSWORD above
      
      REQUIRE_ACCEPT: terms,privacy
      # TODO: adjust these links, they will be shown during the signup!
      LINK_terms: "https://www.werkundkultur.de/nutzungsbedingungen/"
      LINK_privacy: "https://www.werkundkultur.de/impressum/"
      LINK_mail: "mail@werkundkultur.de"
      
      # TODO: set up an SMTP server here for sending email - only required when signups and password resets are enabled
      SMTP_SERVER: "example.org:587"
      SMTP_USERNAME: "..."
      SMTP_PASSWORD: "..."
      SMTP_FROM: "Werk- und Kulturscheune <noreply@werkundkultur.de>"
      
      # For the full documentation, see THE YET-TO-EXIST DOCUMENTATION LINK!
```

## Embedding Peopled into an existing website
Add the following snippet to your website, then adjust `CANONICAL_ROOT_URL` to the page this snippet is on:
```html
<iframe src="data:text/plain;charset=utf-8,JavaScript must be enabled to access peopled!" frameborder="0" width="100%" height="130" id="peopled"></iframe>
<script>
    document.getElementById("peopled").src = "https://account.werkundkultur.de" + location.hash; // TODO: must be the real URL of the peopled UI
    window.addEventListener("message", ev => {
        if (typeof ev.data.peopledScrollHeight !== "undefined") document.getElementById("peopled").height = ev.data.peopledScrollHeight + 2;
        if (ev.data.peopledRemoveHash === true) history.replaceState(null, "", " ");
        if (ev.data.peopledScrollToTop === true) window.scrollTo(0, 0);
    }, false);
</script>
```

## Using the REST API for other applications
The API documentation is available at [Apicurio](https://studio-ws.apicur.io/sharing/12367636-528b-4bd2-839a-ad99efaaa2b0).

## Modify the schema configuration manually
You can use any LDAP schema you want, which is especially useful for existing servers - Peopled won't make any changes to your LDAP schema itself though, so if you want to use custom objectClases, you have to set them up manually. To change the configuration, edit the file `config.go`, which will be copied to your data folder when using the docker-compose setup above.

The full documentation for the internal schema is not yet available.

## Roadmap
- [x] Completion of the LDAP integration
- [x] Adjustment of the core for the LDAP integration
- [x] Check that signup (with email confirmation), login and password reset are working correctly
- [x] Implement a self-service page in the frontend
- [x] Support of the fields from config.go in the frontend
- [ ] Test everything thoroughly, maybe clean up the code a bit
- [ ] Add a users and groups management page in the frontend
- [ ] Add tabs for editing arbitrary LDAP collections
- [ ] Add a way to create the first user (Admin CLI?!)
- [ ] Implement an OAuth2/OIDC server
- [ ] Implement a simple SAML server (for CodiMD, Bookstack, Nextcloud, GitLab)
- [ ] Add 2FA (TOTP & FIDO2) and FIDO2 as a first factor
