module codeberg.org/momar/peopled

go 1.12

require (
	codeberg.org/momar/linkding v0.0.0-20190620121931-acce538672af
	github.com/Masterminds/goutils v1.1.0 // indirect
	github.com/Masterminds/semver v1.5.0 // indirect
	github.com/Masterminds/sprig v2.22.0+incompatible
	github.com/gin-gonic/gin v1.6.2
	github.com/go-asn1-ber/asn1-ber v1.4.1 // indirect
	github.com/go-ldap/ldap/v3 v3.1.8
	github.com/golang/protobuf v1.3.5 // indirect
	github.com/google/uuid v1.1.1 // indirect
	github.com/huandu/xstrings v1.3.0 // indirect
	github.com/imdario/mergo v0.3.9 // indirect
	github.com/jordan-wright/email v0.0.0-20200322182553-8eef2508c362
	github.com/mitchellh/copystructure v1.0.0 // indirect
	github.com/mitchellh/reflectwalk v1.0.1 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/mojocn/base64Captcha v1.3.1
	github.com/rs/zerolog v1.18.0
	github.com/ulule/limiter/v3 v3.5.0
	go.etcd.io/bbolt v1.3.4
	golang.org/x/crypto v0.0.0-20200406173513-056763e48d71 // indirect
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e
	golang.org/x/sys v0.0.0-20200406155108-e3b113bbe6a4 // indirect
	golang.org/x/text v0.3.2
)
