package main

import (
	"codeberg.org/momar/peopled/api"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"net/http"
	"os"
	"strings"
)

func main() {
	app := gin.New()

	api.ActiveConfig = Config
	if uiPath, ok := os.LookupEnv("UI_PATH"); ok {
		log.Info().Msg("mounting UI at /")
		log.Info().Msg("mounting API at /api/")
		api.Setup("/api/", app, gin.WrapH(http.FileServer(http.Dir(uiPath))))
	} else {
		log.Info().Msg("mounting API at / (set UI_PATH to enable the UI)")
		api.Setup("/", app)
	}

	// Stitch together the address of the webserver
	addr := os.Getenv("HOST") + ":" + os.Getenv("PORT")
	if strings.HasSuffix(addr, ":") {
		addr += "80"
	}
	formattedAddr := addr
	if strings.HasPrefix(formattedAddr, ":") {
		formattedAddr = "[::]" + formattedAddr
	}
	formattedAddr = strings.TrimSuffix(formattedAddr, ":80")

	log.Info().Msg("starting webserver at http://" + formattedAddr)
	log.Fatal().Err(app.Run(addr)).Msg("web server exited")
}
