package main

import (
	"bytes"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"os"
	"strings"
)

func init() {
	if strings.ToLower(os.Getenv("LOG_FORMAT")) != "json" {
		log.Logger = zerolog.New(zerolog.NewConsoleWriter(func(w *zerolog.ConsoleWriter) {
			w.PartsOrder = []string{
				zerolog.TimestampFieldName,
				zerolog.LevelFieldName,
				zerolog.MessageFieldName,
				zerolog.CallerFieldName,
			}
			w.TimeFormat = "2006-01-02 15:04:05"
			w.FormatCaller = func(i interface{}) string {
				if strings.ToLower(os.Getenv("LOG_LEVEL")) != "trace" {
					return ""
				}
				s := fmt.Sprint(i)
				s = "caller=" + s[strings.LastIndexByte(s, '/')+1:]
				if w.NoColor {
					return s
				}
				return fmt.Sprintf("\x1b[%dm%v\x1b[0m", 90, s)
			}
		}))
	}

	log.Logger = log.Logger.Hook(zerolog.HookFunc(func(e *zerolog.Event, level zerolog.Level, message string) {
		*e = *(e.Timestamp())
	}))

	gin.SetMode(gin.ReleaseMode)
	switch strings.ToLower(os.Getenv("LOG_LEVEL")) {
	case "trace":
		log.Logger = log.Logger.Level(zerolog.TraceLevel)
		gin.SetMode(gin.DebugMode)
	case "debug":
		log.Logger = log.Logger.Level(zerolog.DebugLevel)
		gin.SetMode(gin.DebugMode)
	case "info":
		log.Logger = log.Logger.Level(zerolog.InfoLevel)
	case "warning":
		log.Logger = log.Logger.Level(zerolog.WarnLevel)
	case "warn":
		log.Logger = log.Logger.Level(zerolog.WarnLevel)
	case "error":
		log.Logger = log.Logger.Level(zerolog.ErrorLevel)
	default:
		log.Logger = log.Logger.Level(zerolog.InfoLevel)
	}

	gin.DefaultWriter = ginWriter{false, log.Logger}
	gin.DefaultErrorWriter = ginWriter{true, log.Logger}

	if strings.ToLower(os.Getenv("LOG_LEVEL")) == "trace" {
		log.Logger = log.Logger.Hook(zerolog.HookFunc(func(e *zerolog.Event, level zerolog.Level, message string) {
			*e = *(e.Caller(4))
		}))
	}
}

type ginWriter struct{
	err bool
	logger zerolog.Logger
}

func (w ginWriter) Write(p []byte) (n int, err error) {
	length := len(p)
	p = bytes.TrimPrefix(p, []byte("[GIN-debug] "))
	p = bytes.TrimSpace(p)
	level := zerolog.DebugLevel
	if w.err {
		level = zerolog.ErrorLevel
	}
	if bytes.HasPrefix(p, []byte("[WARNING] ")) {
		p = bytes.TrimPrefix(p, []byte("[WARNING] "))
		level = zerolog.WarnLevel
		if bytes.HasPrefix(p, []byte("Running in \"debug\" mode.")) {
			return length, nil
		}
	}
	if bytes.HasPrefix(p, []byte("Environment variable PORT is undefined. ")) || bytes.HasPrefix(p, []byte("Listening and serving ")) {
		return length, nil
	}
	lines := bytes.Split(p, []byte("\n"))
	for _, line := range lines {
		w.logger.WithLevel(level).Timestamp().Caller(5).Msgf("%s", line)
	}
	return length, nil
}
