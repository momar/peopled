package main

import (
	"codeberg.org/momar/peopled/api"
	"codeberg.org/momar/peopled/ldap"
	"codeberg.org/momar/peopled/ldap/field"
	"encoding/json"
	"github.com/rs/zerolog/log"
	"io/ioutil"
	"os"
	"regexp"
	"strings"
)

var Domain = "dc=" + strings.TrimPrefix(strings.ReplaceAll(api.EnvOrExit("LDAP_DOMAIN"), ".", ",dc="), "dc=")

var Config = api.Config{
	ListenAddress:             ":8080",
	UserResource:              "user",
	CanonicalRootURL:          api.EnvOrExit("CANONICAL_ROOT_URL"),
	RequireAccept:             api.EnvSliceOrEmptySlice("REQUIRE_ACCEPT"),
	Links:                     Links(),
	RequireEmailConfirmation:  api.EnvIsTrue("REQUIRE_EMAIL_CONFIRMATION", true),
	RequireAdminConfirmation:  api.EnvIsTrue("REQUIRE_ADMIN_CONFIRMATION", false),
	AdminConfirmationEmails:   api.EnvSliceOrEmptySlice("ADMIN_CONFIRMATION_EMAILS"),
	AdminConfirmationGroups:   api.EnvSliceOrEmptySlice("ADMIN_CONFIRMATION_GROUPS"),
	AdminConfirmationLanguage: api.EnvOrFallback("ADMIN_CONFIRMATION_LANG", "en"),
	EnableSignup:              api.EnvIsTrue("ENABLE_SIGNUP", true),
	EnableSignupCaptcha:       api.EnvIsTrue("ENABLE_SIGNUP_CAPTCHA", true),
	EnableRecovery:            api.EnvIsTrue("ENABLE_RECOVERY", true),
	EmailConfig: api.EmailConfig{
		Server:             api.EnvOrFallback("SMTP_SERVER", ""),
		Username:           api.EnvOrFallback("SMTP_USERNAME", ""),
		Password:           api.EnvOrFallback("SMTP_PASSWORD", ""),
		FromAddress:        api.EnvOrFallback("SMTP_FROM", ""),
		UsePlainTLS:        api.EnvIsTrue("SMTP_USE_PLAIN_TLS", false),
		InsecureSkipVerify: api.EnvIsTrue("SMTP_INSECURE_SKIP_VERIFY", false),
	},
	PluginConfig: []api.PluginConfig{
		ldap.Config{
			ConnectionURI: api.EnvOrFallback("LDAP_ADDRESS", "ldap://127.0.0.1:389"),
			BindUsername:  api.EnvOrFallback("LDAP_USERNAME", "cn=admin," + Domain),
			BindPassword:  api.EnvOrExit("LDAP_PASSWORD"),
			TrustInsecure: api.EnvIsTrue("LDAP_TRUST_INSECURE", false),
		},
	},
	Apps: Apps(),
	Tabs: Tabs(),
	Resources: map[string]api.ResourceType{
		"user": &ldap.ResourceType{
			SearchBase:   "ou=users," + Domain,
			SearchFilter: "",

			ObjectClasses:   []string{"inetOrgPerson", "posixAccount"},
			DNTemplate:      "uid=%s,ou=users," + Domain,
			IdentifierField: "uid",
			EmailField:      "mail",
			MemberOfField:   "memberOf",
			NameField:       "cn",
			PasswordField:   "userPassword",

			Views: api.ResourceViews{
				List:   []string{"user"},
				Create: []string{"group:admin"},
				Delete: []string{"group:admin"},
			},

			Fields: []ldap.Field{
				&field.MemberOf{
					Attribute:  "memberOf",
					DNTemplate: "cn=%s,ou=groups," + Domain,
					Views: field.Views{
						Read:   []string{"user"},
						Update: []string{"group:admin"},
					},
				},
				&field.Text{
					Attribute:  "uid",
					Identifier: true,
					Views: field.Views{
						Read:   []string{"user"},
						Update: []string{"group:admin"},
					},
					Title: api.T("en", "username:").T("de", "Benutzername:"),
					Icon: "ri-user-fill",
					Validations: []*regexp.Regexp{regexp.MustCompile(`^[a-zA-Z][a-zA-Z0-9-_.]{2,32}$`)},
				},
				&field.Count{
					Attribute: "uidNumber",
					Offset:    1000,
					Resource:  "user",
				},
				&field.Email{
					Attribute:  "mail",
					Identifier: true,
					Views: field.Views{
						Read:   []string{"user"},
						Update: []string{"group:admin", "selfservice"},
					},
					Title: api.T("en", "email:").T("de", "E-Mail-Adresse:"),
					MinCount: 1,
				},
				&field.Password{
					Attribute: "userPassword",
					Views: field.Views{
						Read:   []string{},
						Update: []string{"group:admin", "selfservice"},
					},
					Title: api.T("en", "set new password:").T("de", "Neues Passwort setzen:"),
					Icon: "ri-lock-password-fill",
					//Complexity: 2,
					MinLength:  8,
				},
				&field.Picture{
					Attribute: "jpegPhoto",
					Views: field.Views{
						Read:   []string{"user"},
						Update: []string{"group:admin", "selfservice"},
					},
					Title: api.T("en", "profile picture:").T("de", "Profilbild:"),
				},
				&field.Fixed{
					Attribute: "givenName",
					Value:     `{{regexReplaceAll " [^ ]+$|^[^ ]+$" .cn ""}}`,
					Views: field.Views{
						Read:   []string{"user"},
						Update: []string{},
					},
				},
				&field.Fixed{
					Attribute: "sn",
					Value:     `{{regexReplaceAll "^.* " .cn ""}}`,
					Views: field.Views{
						Read:   []string{"user"},
						Update: []string{},
					},
				},
				&field.Text{
					Attribute: "cn",
					Title: api.T("en", "name:").T("de", "Vor- und Nachname:"),
					Icon: "ri-user-fill",
					Views: field.Views{
						Read:   []string{"user"},
						Update: []string{"group:admin", "selfservice"},
					},
				},
				&field.Fixed{
					Attribute: "gidNumber",
					Value:     "500",
					Views: field.Views{
						Read:   []string{},
						Update: []string{},
					},
				},
				&field.Fixed{
					Attribute: "homeDirectory",
					Value:     "/home/users/{{.uid}}",
					Views: field.Views{
						Read:   []string{},
						Update: []string{},
					},
				},
			},
		},
		"group": &ldap.ResourceType{
			SearchBase:   "ou=groups," + Domain,
			SearchFilter: "",

			ObjectClasses:   []string{"groupOfNames"},
			DNTemplate:      "cn=%s,ou=groups," + Domain,
			IdentifierField: "cn",

			Views: api.ResourceViews{
				List:   []string{"user"},
				Create: []string{"group:admin"},
				Delete: []string{"group:admin"},
			},

			Fields: []ldap.Field{
				&field.Text{
					Attribute:  "cn",
					Identifier: true,
					Views: field.Views{
						Read:   []string{"user"},
						Update: []string{"group:admin"},
					},
				},
				&field.Text{
					Attribute: "description",
					Views: field.Views{
						Read:   []string{"user"},
						Update: []string{"group:admin"},
					},
					Long: true,
				},
				/*&field.Members{
					Attribute: "uniqueMember",
					Views: field.Views{
						Read: []string{"user"},
						Update: []string{"group:admin"},
					},
				},*/
			},
		},
	},
}

func Apps() []api.App {
	path := api.EnvOrFallback("APPS_PATH", "apps.json")
	data, err := ioutil.ReadFile(path)
	if err == os.ErrNotExist {
		return []api.App{}
	} else if err != nil {
		log.Fatal().Str("path", path).Err(err).Msg("couldn't load app configuration file")
	}
	apps := []api.App{}
	err = json.Unmarshal(data, &apps)
	if err != nil {
		log.Fatal().Str("path", path).Err(err).Msg("couldn't parse app configuration file")
	}
	return apps
}

func Tabs() []api.Tab {
	path := api.EnvOrFallback("TABS_PATH", "tabs.json")
	data, err := ioutil.ReadFile(path)
	if err == os.ErrNotExist {
		return []api.Tab{
			{Name: api.T("en", "My Account").T("de", "Mein Account"), Icon: "ri-user-settings-line", Type: "selfservice"},
			{Name: api.T("en", "Users").T("de", "Benutzer"), Icon: "ri-group-line", Type: "resources", ResourceType: "user", Views: []string{"group:admin"}},
			{Name: api.T("en", "Groups").T("de", "Gruppen"), Icon: "ri-team-line", Type: "resources", ResourceType: "group", Views: []string{"group:admin"}},
			{Name: api.T("en", "Logout").T("de", "Abmelden"), Icon: "ri-logout-box-r-line", Type: "logout"},
		}
	} else if err != nil {
		log.Fatal().Str("path", path).Err(err).Msg("couldn't load tab configuration file")
	}
	tabs := []api.Tab{}
	err = json.Unmarshal(data, &tabs)
	if err != nil {
		log.Fatal().Str("path", path).Err(err).Msg("couldn't parse tab configuration file")
	}
	return tabs
}

func Links() map[string]string {
	links := map[string]string{}
	environment := os.Environ()
	for _, env := range environment {
		parts := strings.SplitN(env, "=", 2)
		if strings.HasPrefix(strings.ToLower(parts[0]), "link_") && len(parts) > 1 {
			links[parts[0][5:]] = parts[1]
		}
	}
	return links
}
