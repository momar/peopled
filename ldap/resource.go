package ldap

import (
	"codeberg.org/momar/peopled/api"
	"errors"
	"fmt"
	"github.com/go-ldap/ldap/v3"
	"github.com/rs/zerolog/log"
	"strings"
)

// Resource is an instance of ResourceType, e.g. "the user Max". It must be created by calling ResourceType.New().
type Resource struct {
	Type   *ResourceType // The class this object is an instance of.
	Fields []Field       // The list of fields this object can have.
}

var _ api.Resource = &Resource{}

func (res *Resource) applyTemplates() map[string]error {
	errs := map[string]error{}
	for _, field := range res.Fields {
		if updatableField, ok := field.(UpdatableField); ok {
			if err := updatableField.ApplyTemplates(FieldsToJSON(res.Fields)); err != nil {
				errs[field.Name()] = err
			}
		}
	}
	if len(errs) > 0 {
		return errs
	}
	return nil
}

// Validate & apply the request body of a POST or PUT request & populate the ID
func (res *Resource) FromBody(body map[string]interface{}, viewpoint api.Viewpoint) map[string]error {
	errs := map[string]error{}
	for name, value := range body {
		field := res.Field(name)
		if field == nil {
			errs[name] = errors.New("superfluous field")
		} else if !viewpoint.Check(field.GetViews().Update) {
			errs[field.Name()] = errors.New("not writable from your viewpoint")
		} else if err := field.FromJSON(value); err != nil {
			errs[field.Name()] = err
		}
	}
	if len(errs) > 0 {
		return errs
	}

	errs = res.applyTemplates()
	if len(errs) > 0 {
		return errs
	}

	return nil
}

// Retrieve a response body for a GET request (calls read() and then converts it to a JSON-compatible format)
func (res *Resource) ToBody(viewpoint api.Viewpoint) (map[string]interface{}, error) {
	result := map[string]interface{}{}
	for _, field := range res.Fields {
		if viewpoint.Check(field.GetViews().Read) {
			result[field.Name()] = field.ToJSON()
		}
	}
	if len(result) < 1 {
		return nil, api.ErrMissingPermission
	}
	return result, nil
}

func (res *Resource) Read(id string) error {
	if err := ValidateDN(id); err != nil {
		return fmt.Errorf("(400) %w", err)
	}
	return res.read(id, true)
}
func (res *Resource) read(id string, strict bool) error {
	attributes := []string{}
	for _, field := range res.Fields {
		if fieldAttr, ok := field.(interface{ Attributes() []string }); ok {
			attributes = append(attributes, fieldAttr.Attributes()...)
		} else {
			attributes = append(attributes, field.Name())
		}
	}

	// read from LDAP
	var ldapRequest *ldap.SearchRequest
	if !strict {
		filter := "(|"
		for _, field := range res.Fields {
			if filterField, ok := field.(FilterField); ok {
				identification := filterField.Identify(id)
				if identification != "" {
					filter += identification
				}
			}
		}
		filter += ")"
		if filter == "(|)" {
			strict = true // fall back to strict check
		} else {
			filter = "(&" + res.Type.SearchFilter + filter + ")"
			log.Trace().Str("filter", filter).Msg("using filtered request")
			ldapRequest = ldap.NewSearchRequest(
				res.Type.SearchBase,
				ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 1, 30, false,
				filter,
				attributes,
				nil,
			)
		}
	}
	if strict { // not an "else", as an error in a non-strict request shall trigger a strict request
		dn := strings.Replace(res.Type.DNTemplate, "%s", id, 1)
		log.Trace().Str("dn", dn).Msg("using strict request")
		ldapRequest = ldap.NewSearchRequest(
			dn,
			ldap.ScopeBaseObject, ldap.NeverDerefAliases, 1, 30, false,
			res.Type.SearchFilter,
			attributes,
			nil,
		)
	}
	ldapResult, err := Connection.Search(ldapRequest)
	if err != nil && strings.HasPrefix(err.Error(), "LDAP Result Code 32 \"No Such Object\": ") {
		log.Trace().Err(err).Msg("search done")
		return api.ErrResourceDoesntExist
	}
	if err != nil {
		log.Trace().Err(err).Msg("search done")
		return err
	}
	if ldapResult == nil {
		log.Trace().Int("count", 0).Msg("search done")
		return api.ErrResourceDoesntExist
	}
	if !strict {
		// clean out all entries that don't match the DN template
		for i := 0; i < len(ldapResult.Entries); i++ {
			if !res.Type.MatchDN(ldapResult.Entries[i].DN) {
				ldapResult.Entries = append(ldapResult.Entries[:i], ldapResult.Entries[i+1:]...)
				i--
			}
		}
	}
	log.Trace().Int("count", len(ldapResult.Entries)).Msg("search done")
	if len(ldapResult.Entries) < 1 {
		return api.ErrResourceDoesntExist
	}

	return res.readLDAP(ldapResult.Entries[0])
}
func (res *Resource) readLDAP(entry *ldap.Entry) error {
	// get the "source" variable populated
	var source = map[string][]string{}
	for _, attr := range entry.Attributes {
		source[attr.Name] = attr.Values
	}

	// process fields
	for i := range res.Fields {
		if err := res.Fields[i].FromLDAP(source); err != nil {
			return err
		}
	}

	if err := res.applyTemplates(); err != nil && len(err) > 0 {
		// return the first error
		for _, err := range err {
			return err
		}
	}

	return nil
}

func (res *Resource) Create(id string) error {
	ldapRequest := ldap.NewAddRequest(res.Type.FormatDN(id), []ldap.Control{})
	ldapRequest.Attribute("objectClass", res.Type.ObjectClasses)

	// Merge data
	attributes := map[string][]string{}
	for _, field := range res.Fields {

		for attr, value := range field.ToLDAP() {
			attributes[attr] = value
		}
	}

	log.Debug().Str("dn", res.Type.FormatDN(id)).Interface("attributes", attributes).Strs("objectClass", res.Type.ObjectClasses).Msg("creating resource")

	// Add data attributes
	for attr, value := range attributes {
		if len(value) > 0 {
			ldapRequest.Attribute(attr, value)
		}
	}

	return Connection.Add(ldapRequest)

	// TODO: what if the resource already exists?
}

func (res *Resource) Update(id string) error {
	// Check that the resource exists
	old := res.Type.New().(*Resource)
	err := old.Read(id)
	if err != nil {
		return err
	}

	// Check that there's no resource with the new ID (if it has changed)
	if id != res.ID() {
		ldapRequest := ldap.NewSearchRequest(
			strings.Replace(res.Type.DNTemplate, "%s", id, 1),
			ldap.ScopeBaseObject, ldap.NeverDerefAliases, 1, 0, false,
			"",
			nil,
			nil,
		)
		ldapResult, err := Connection.Search(ldapRequest)
		if err != nil {
			return err
		}
		if !(ldapResult == nil || len(ldapResult.Entries) < 1) {
			return api.ErrResourceExists
		}
	}

	ldapRequest := ldap.NewModifyRequest(res.Type.FormatDN(id), []ldap.Control{})

	// Merge data
	newAttributes := FieldsToLDAP(res.Fields)
	oldAttributes := FieldsToLDAP(old.Fields)

	// TODO: allow editing memberOf fields!
	// TODO: unique fields like mail?

	// Upodate data attributes
	for attr, value := range newAttributes {
		if _, ok := oldAttributes[attr]; ok {
			ldapRequest.Replace(attr, value)
		} else {
			ldapRequest.Add(attr, value)
		}
	}
	for attr, value := range oldAttributes {
		if _, ok := newAttributes[attr]; !ok {
			ldapRequest.Delete(attr, value)
		}
	}

	err = Connection.Modify(ldapRequest)
	return err
}

// Delete the resource
func (res *Resource) Delete(id string, ctx api.Viewpoint) error {
	if !ctx.Check(res.Type.Views.Delete) {
		return api.ErrMissingPermission
	}
	ldapRequest := ldap.NewDelRequest(res.Type.FormatDN(id), []ldap.Control{})
	err := Connection.Del(ldapRequest)
	if err != nil && strings.HasPrefix(err.Error(), "LDAP Result Code 32 \"No Such Object\": ") {
		return api.ErrResourceDoesntExist
	}
	return err
}

// Field returns a field by its name
func (res *Resource) Field(name string) Field {
	for _, field := range res.Fields {
		if field.Name() == name {
			return field
		}
	}
	return nil
}

func (res *Resource) ID() string {
	return fmt.Sprint(res.Field(res.Type.IdentifierField).ToJSON())
}
