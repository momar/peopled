package ldap

import (
	"codeberg.org/momar/peopled/api"
	"errors"
	"fmt"
	"github.com/go-ldap/ldap/v3"
	"github.com/rs/zerolog/log"
	"strings"
)

// ResourceType represents a resource type containing LDAP objects (e.g. users).
type ResourceType struct {
	SearchBase      string
	SearchFilter    string
	DNTemplate      string   // A text/template for the object DNs (with the LDAP object's representation as the root context).
	ObjectClasses   []string // The LDAP object classes for objects of this type.
	Views           api.ResourceViews
	NameField       string
	EmailField      string
	MemberOfField   string
	IdentifierField string
	PasswordField   string
	Fields          []Field // The field types objects of this class can have.
}

var _ api.AuthenticationResourceType = &ResourceType{}

func (resType *ResourceType) Setup() error {
	if resType.SearchBase == "" {
		return errors.New("SearchBase is required")
	}
	if resType.DNTemplate == "" || !strings.Contains(resType.DNTemplate, "%s") {
		return errors.New("DNTemplate is required & must contain a placeholder (%s)")
	}
	if resType.ObjectClasses == nil || len(resType.ObjectClasses) < 1 {
		return errors.New("ObjectClasses is required")
	}
	if resType.Fields == nil || len(resType.Fields) < 1 {
		return errors.New("FieldTypes is required")
	}
	if resType.SearchFilter == "" {
		resType.SearchFilter = "(objectClass=*)"
	}

	var err error
	var identifierFieldFound bool
	for i := range resType.Fields {
		err = resType.Fields[i].Setup()
		if resType.Fields[i].Name() == resType.IdentifierField {
			identifierFieldFound = true
		}
		if err != nil {
			return err
		}
	}
	if !identifierFieldFound {
		return errors.New("IdentifierField doesn't exist, but is required")
	}

	// Create OUs for this resource type
	dcs := true
	ous := strings.Split(resType.SearchBase, ",")
	for i := len(ous) - 1; i >= 0; i-- {
		if strings.HasPrefix(strings.ToLower(ous[i]), "dc=") && dcs {
			continue
		}
		dcs = false

		parts := strings.SplitN(ous[i], "=", 2)
		if len(parts) < 2 || strings.ToLower(parts[0]) != "ou" {
			return errors.New("SearchBase is not a valid LDAP DN (only OU and DC are allowed, DC must be the last parts)")
		}

		ouCreateRequest := ldap.NewAddRequest(strings.Join(ous[i:], ","), []ldap.Control{})
		ouCreateRequest.Attribute("objectClass", []string{"organizationalUnit"})
		ouCreateRequest.Attribute("ou", []string{UnescapeDN(parts[1])})
		if Connection == nil {
			return errors.New("not yet connected")
		}
		err := Connection.Add(ouCreateRequest)
		if err != nil && !strings.HasPrefix(err.Error(), "LDAP Result Code 68 \"Entry Already Exists\": ") {
			return err
		}
	}

	return nil
}

// New creates an instance of this class.
func (resType *ResourceType) New() api.Resource {
	out := &Resource{
		Type:   resType,
		Fields: make([]Field, len(resType.Fields)),
	}

	for i := range out.Fields {
		out.Fields[i] = out.Type.Fields[i].New()
	}

	return out
}

func (resType *ResourceType) Schema() []api.FieldSchema {
	result := make([]api.FieldSchema, len(resType.Fields))
	for i, fieldType := range resType.Fields {
		result[i] = fieldType.Schema()
	}
	return result
}

func (resType *ResourceType) ListCheck(viewpoint api.Viewpoint) error {
	// Check the permissions
	if !viewpoint.Check(resType.Views.List) {
		return api.ErrMissingPermission
	}
	return nil
}

func (resType *ResourceType) List(query api.Query) ([]string, error) {
	// Search the whole subtree for objects
	req := ldap.NewSearchRequest(
		resType.SearchBase,
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 10, false,
		resType.SearchFilter,
		nil,
		nil,
	)
	res, err := Connection.Search(req)
	if err != nil {
		return nil, err
	} else if res == nil || len(res.Entries) < 1 {
		return []string{}, nil
	}

	// Put result together - consider elements that match the DN template and the query
	result := []string{}
Entry:
	for _, entry := range res.Entries {
		if !resType.MatchDN(entry.DN) {
			log.Trace().Err(err).Msg("resource found in subtree that doesn't match the DN template")
		} else {
			if len(query) > 0 {
				queryTester := resType.New().(*Resource)
				err := queryTester.readLDAP(entry)
				if err != nil {
					continue
				}
				for field, value := range query {
					fieldValue := queryTester.Field(field).ToJSON()
					if s, ok := fieldValue.(string); ok {
						if !value.Match(s) {
							continue Entry
						}
					} else if a, ok := fieldValue.([]string); ok {
						found := false
						for _, s := range a {
							if value.Match(s) {
								found = true
								break
							}
						}
						if !found {
							continue Entry
						}
					} else {
						continue Entry
					}
				}
			}
			result = append(result, resType.UnwrapDN(entry.DN))
		}
	}

	return result, nil
}

func (resType *ResourceType) Authenticate(username string, password string) (api.User, error) {
	// Check if the user exists (and grab his details)
	user := resType.New().(*Resource)
	err := user.read(username, false)
	if err == api.ErrResourceDoesntExist {
		return api.User{}, api.ErrUserDoesntExist
	} else if err != nil {
		return api.User{}, fmt.Errorf("couldn't check username: %w", err)
	}

	// Check if the password's correct
	if password != "" {
		req := ldap.NewSimpleBindRequest(resType.FormatDN(user.ID()), password, nil)
		res, err := SimpleConnection.SimpleBind(req)
		if err != nil && strings.HasPrefix(err.Error(), "LDAP Result Code 49 \"Invalid Credentials\":") {
			return api.User{}, api.ErrIncorrectPassword
		} else if err != nil {
			return api.User{}, fmt.Errorf("couldn't check password: %w", err)
		} else if res == nil {
			return api.User{}, errors.New("couldn't check: unknown error")
		}
	}

	// build user object
	var memberOf []string
	memberOfField := user.Field(user.Type.MemberOfField)
	if user.Type.MemberOfField != "" && memberOfField == nil {
		return api.User{}, errors.New("MemberOfField doesn't exist and is not empty")
	} else if memberOfField.Schema().Type != "memberOf" {
		return api.User{}, errors.New("MemberOfField is not a memberOf field")
	} else if memberOfField != nil {
		memberOf = memberOfField.ToJSON().([]string)
	}

	name := ""
	nameField := user.Field(user.Type.NameField)
	if user.Type.NameField != "" && nameField == nil {
		return api.User{}, errors.New("NameField doesn't exist and is not empty")
	} else if nameField.Schema().Type != "text" {
		return api.User{}, errors.New("NameField is not a text field")
	} else if nameField != nil {
		name = nameField.ToJSON().(string)
	}

	var email []string
	emailField := user.Field(user.Type.EmailField)
	if user.Type.EmailField != "" && emailField == nil {
		return api.User{}, errors.New("EmailField doesn't exist and is not empty")
	} else if emailField.Schema().Type != "email" {
		return api.User{}, errors.New("EmailField is not an email field")
	} else if emailField != nil {
		email = emailField.ToJSON().([]string)
	}

	return api.User{
		UID:      user.ID(),
		MemberOf: memberOf,
		Name:     name,
		Email:    email,
	}, nil
}

func (resType *ResourceType) SetPassword(username string, newPassword string) error {
	// Check if the user exists (and grab his details)
	user := resType.New().(*Resource)
	err := user.read(username, false)
	if err == api.ErrResourceDoesntExist {
		return api.ErrUserDoesntExist
	} else if err != nil {
		return fmt.Errorf("couldn't check username: %w", err)
	}

	err = user.Field(resType.PasswordField).FromJSON(newPassword)
	if err != nil {
		return fmt.Errorf("(400) %w", err)
	}

	err = user.Update(username)
	if err != nil {
		return fmt.Errorf("(500) %w", err)
	}
	return nil
}

func (resType *ResourceType) Signup(signup api.Signup, validateOnly bool) map[string]error {
	res := resType.New().(*Resource)
	errs := map[string]error{}

	identifierField := res.Field(resType.IdentifierField)
	if identifierField == nil {
		errs["username"] = fmt.Errorf("not supported (is the UserResource a user resource?)")
	} else {
		err := identifierField.FromJSON(signup.Username)
		if err != nil {
			errs["username"] = err
		}
	}

	passwordField := res.Field(resType.PasswordField)
	if passwordField == nil {
		errs["password"] = fmt.Errorf("not supported (is the UserResource a user resource?)")
	} else {
		err := passwordField.FromJSON(signup.Password)
		if err != nil {
			errs["password"] = err
		}
	}

	emailField := res.Field(resType.EmailField)
	if emailField == nil && signup.Email != "" {
		errs["email"] = fmt.Errorf("not supported")
	} else if emailField != nil {
		err := emailField.FromJSON(signup.Email)
		if err != nil {
			errs["email"] = err
		}
	}

	nameField := res.Field(resType.NameField)
	if nameField == nil && signup.Name != "" {
		errs["name"] = fmt.Errorf("not supported")
	} else if nameField != nil {
		err := nameField.FromJSON(signup.Name)
		if err != nil {
			errs["name"] = err
		}
	}

	if len(errs) > 0 {
		return errs
	}

	errs = res.applyTemplates()
	if errs != nil && len(errs) > 0 {
		return errs
	}
	errs = map[string]error{}

	if !validateOnly {
		// TODO: check if the user exists => different error code?!
		errs["500"] = res.Create(signup.Username)
		if errs["500"] != nil {
			return errs
		}
	}

	return nil
}

func (resType *ResourceType) FormatDN(id string) string {
	return strings.Replace(resType.DNTemplate, "%s", EscapeDN(id), 1)
}
func (resType *ResourceType) UnwrapDN(dn string) string {
	index := strings.Index(resType.DNTemplate, "%s")
	return UnescapeDN(strings.TrimSuffix(strings.TrimPrefix(dn, resType.DNTemplate[:index]), resType.DNTemplate[index+2:]))
}
func (resType *ResourceType) MatchDN(dn string) bool {
	index := strings.Index(resType.DNTemplate, "%s")
	return len(dn) >= len(resType.DNTemplate)-1 && strings.HasPrefix(dn, resType.DNTemplate[:index]) && strings.HasSuffix(dn, resType.DNTemplate[index+2:])
}
