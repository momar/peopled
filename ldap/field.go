package ldap

import "codeberg.org/momar/peopled/api"

// Field represents an instance of a field, like "Max's main email address". It must be created by calling LDAPType.New().
type Field interface {
	Setup() error
	New() Field              // Create a new field from this type. Throws an error on configuration problems.
	Name() string            // Get the user-specified unique name of this field, for the frontend API and errors/logging.
	Schema() api.FieldSchema // Get the frontend format instructions in JSON format - field.FromJSON(field.ToJSON().Value) should not change the value.
	GetViews() api.FieldViews

	FromLDAP(map[string][]string) error // Override the field's value from a source with LDAP format - when using field.FromLDAP(field.ToLDAP()), the result should be the same as before. Should throw an error only if the LDAP is in an invalid state.
	ToLDAP() map[string][]string        // Get the LDAP fields representing this field's value - when using field.FromLDAP(field.ToLDAP()), the result should be the same as before.
	FromJSON(interface{}) error         // Override the field's value from a JSON value - field.FromJSON(field.ToJSON().Value) should not change the value. Should throw an error if the value is invalid.
	ToJSON() interface{}                // Get the frontend-formatted value
}

type UpdatableField interface {
	Field
	ApplyTemplates(context map[string]interface{}) error // Should be used to apply templates based on other fields after everything's done. Note that referencing other UpdatableFields might lead to issues.
}

type FilterField interface {
	Field
	Identify(string) string
}
