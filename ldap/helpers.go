package ldap

import (
	"errors"
	"fmt"
	"strings"
)

func FieldsToLDAP(fields []Field) map[string][]string {
	attributes := map[string][]string{}
	for _, field := range fields {
		for name, value := range field.ToLDAP() {
			attributes[name] = value
		}
	}
	return attributes
}

func FieldsToJSON(fields []Field) map[string]interface{} {
	result := map[string]interface{}{}
	for _, field := range fields {
		result[field.Name()] = field.ToJSON()
	}
	return result
}

func ValidateDN(dn string) error {
	for _, b := range []byte(dn) {
		if b < 0x20 || b == 0x7F {
			return errors.New("DN contains binary characters")
		}
	}
	return nil
}

func EscapeSearch(dn string) string {
	// http://blog.dzhuvinov.com/?p=585
	dn = strings.ReplaceAll(dn, `*`, `\2a`)
	dn = strings.ReplaceAll(dn, `(`, `\,`)
	dn = strings.ReplaceAll(dn, `)`, `\#`)
	dn = strings.ReplaceAll(dn, `\`, `\+`)
	for i := 0; i < len(dn); i++ {
		if dn[i] >= 0x80 {
			dn = dn[:i] + fmt.Sprintf("\\%2x", dn[i]) + dn[i+1:]
			i += 2
		}
	}
	return dn
}

func EscapeDN(dn string) string {
	dn = strings.ReplaceAll(dn, `\`, `\\`)
	dn = strings.ReplaceAll(dn, `,`, `\,`)
	dn = strings.ReplaceAll(dn, `#`, `\#`)
	dn = strings.ReplaceAll(dn, `+`, `\+`)
	dn = strings.ReplaceAll(dn, `<`, `\<`)
	dn = strings.ReplaceAll(dn, `>`, `\>`)
	dn = strings.ReplaceAll(dn, `;`, `\;`)
	dn = strings.ReplaceAll(dn, `"`, `\"`)
	dn = strings.ReplaceAll(dn, `=`, `\=`)
	dn = strings.TrimSpace(dn) // We could escape leading/trailing spaces too, but this is stupid for usernames & groups.
	return dn
}

func UnescapeDN(dn string) string {
	dn = strings.ReplaceAll(dn, `\\`, "\x00")
	dn = strings.ReplaceAll(dn, `\,`, `,`)
	dn = strings.ReplaceAll(dn, `\#`, `#`)
	dn = strings.ReplaceAll(dn, `\+`, `+`)
	dn = strings.ReplaceAll(dn, `\<`, `<`)
	dn = strings.ReplaceAll(dn, `\>`, `>`)
	dn = strings.ReplaceAll(dn, `\;`, `;`)
	dn = strings.ReplaceAll(dn, `\"`, `"`)
	dn = strings.ReplaceAll(dn, `\=`, `=`)
	dn = strings.ReplaceAll(dn, "\x00", `\`)
	return dn
}
