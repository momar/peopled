package ldap

import (
	"crypto/tls"
	"github.com/go-ldap/ldap/v3"
	"github.com/rs/zerolog/log"
	"time"
)

type Config struct {
	ConnectionURI string
	TrustInsecure bool
	BindUsername  string
	BindPassword  string
}

// Connection is used to access the LDAP directory tree for read & write access.
var Connection *ldap.Conn

// SimpleConnection is used to authenticate users using a SimpleBind.
var SimpleConnection *ldap.Conn

var firstSetup = true

func (config Config) SetupPlugin() error {
	var opts = []ldap.DialOpt{}
	if config.TrustInsecure {
		opts = append(opts, ldap.DialWithTLSConfig(&tls.Config{
			InsecureSkipVerify: true,
		}))
	}

	var err error
	c, err := ldap.DialURL(config.ConnectionURI, opts...)
	if err != nil {
		return err
	}
	err = c.Bind(config.BindUsername, config.BindPassword)
	if err != nil {
		return err
	}
	Connection = c

	SimpleConnection, err = ldap.DialURL(config.ConnectionURI, opts...)
	if err != nil {
		return err
	}

	log.Info().Msg("Successfully connected to LDAP Server.")
	
	if firstSetup {
		firstSetup = false
		go func() {
			t := time.NewTicker(2 * time.Second)
			for {
				<-t.C
				if Connection.IsClosing() || SimpleConnection.IsClosing() {
					log.Warn().Msg("LDAP connection failed, reconnection to LDAP...")
					if err := config.SetupPlugin(); err != nil {
						log.Error().Err(err).Msg("LDAP reconnection failed.")
					}
				}
			}
		}()
	}
	return nil
}
