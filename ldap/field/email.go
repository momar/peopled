package field

import (
	"codeberg.org/momar/peopled/api"
	"codeberg.org/momar/peopled/ldap"
	"fmt"
	"golang.org/x/net/idna"
	"net"
	"regexp"
	"strings"
	"unicode"
)

// Email is an email address of a user
type Email struct {
	Attribute
	Views
	Required
	Identifier bool
	Title        api.TranslatedString
	Icon         string
	MaxCount   uint
	MinCount   uint // You probably want to set this to 1 to require at least 1 email address!
	//RequireValidation bool
	value []string
}

var _ ldap.Field = &Email{}

// EmailHostnameRegex must be matched by the domain part of an email address, converted to its lowercase ASCII
// representation (no punycode), with a trailing dot for easier matching. There is no EmailLocalRegex, as we're just
// using two simple rules to catch typos: only one @ allowed, and no spaces.
var EmailHostnameRegex = regexp.MustCompile(`^([a-z\d]([a-z\d\-]{0,61}[a-z\d])?\.)+$`)

func (field *Email) Setup() error {
	if field.Icon == "" {
		field.Icon = "ri-mail-fill"
	}
	return nil
}

func (field *Email) New() ldap.Field {
	return &Email{
		field.Attribute,
		field.Views,
		field.Required,
		field.Identifier,
		field.Title,
		field.Icon,
		field.MaxCount,
		field.MinCount,
		//class.RequireValidation,
		[]string{},
	}
}

func (field *Email) FromLDAP(in map[string][]string) error {
	if email, ok := in[field.Name()]; !ok {
		field.value = []string{}
	} else {
		field.value = email[:]
	}
	return nil
}

func (field *Email) ToLDAP() map[string][]string {
	return map[string][]string{field.Name(): field.value}
}

func (field *Email) parseJSON(in interface{}) ([]string, error) {
	emails, ok := in.([]string)
	if !ok {
		if emailString, ok := in.(string); ok {
			emails = []string{emailString}
		} else if emailsInterface, ok := in.([]interface{}); ok {
			emails = make([]string, len(emailsInterface))
			for i, emailInterface := range emailsInterface {
				if email, ok := emailInterface.(string); ok {
					emails[i] = email
				} else {
					return nil, fmt.Errorf("must be a string or a string array")
				}
			}
		} else {
			return nil, fmt.Errorf("must be a string or a string array")
		}
	}
	return emails, nil
}
func (field *Email) applyJSON(emails []string) error {
	// validate MinCount and MaxCount
	if uint(len(emails)) < field.MinCount && field.MinCount > 0 {
		if field.MinCount == 1 {
			return fmt.Errorf("an email address is required", field.MinCount)
		}
		return fmt.Errorf("at least %d email addresses are required", field.MinCount)
	}
	if uint(len(emails)) > field.MaxCount && field.MaxCount > 0 {
		if field.MinCount == 1 {
			return fmt.Errorf("only a single email address is allowed")
		}
		return fmt.Errorf("only %d email addresses are allowed", field.MaxCount)
	}

	// validate that emails all have a valid format
	for _, v := range emails {
		parts := strings.Split(v, "@")
		// local part
		if len(parts) != 2 || len(parts[0]) == 0 || strings.IndexFunc(parts[0], unicode.IsSpace) >= 0 {
			return fmt.Errorf("invalid email address (before the @)")
		}
		// domain part
		domain, err := idna.ToASCII(parts[1])
		if err != nil || !EmailHostnameRegex.MatchString(strings.ToLower(domain)+".") {
			return fmt.Errorf("invalid email address (after the @)")
		}
		// mx server
		if mx, err := net.LookupMX(domain); err != nil || len(mx) < 1 {
			return fmt.Errorf("invalid email address (domain doesn't have a mail server)")
		}
	}

	// TODO: validate emails if RequireValidations is set?

	// TODO: add an option RequireUnique that checks if another account with this email exists?! Or maybe in the Update function? Or maybe generally for all fields?

	field.value = emails[:]
	return nil
}

func (field *Email) FromJSON(in interface{}) error {
	emails, err := field.parseJSON(in)
	if err != nil {
		return err
	}
	return field.applyJSON(emails)
}

func (field *Email) ToJSON() interface{} {
	return field.value
}

func (field *Email) Schema() api.FieldSchema {
	return api.FieldSchema{
		Name: field.Name(),
		Type: "email",
		Options: map[string]interface{}{
			"multiple":     field.MaxCount > 1,
			"autocomplete": "email",
			"spellcheck":   false,
			"readonly":     false,
			"title":        field.Title,
			"icon":         field.Icon,
			"minCount": field.MinCount,
			"maxCount": field.MaxCount,
			"required": field.Required,
			//"RequireValidation": field.RequireValidation,
		},
		ReadViews: field.Views.Read,
		WriteViews: field.Views.Update,
	}
}

func (field *Email) Identify(id string) string {
	if field.Identifier {
		return "(" + field.Name() + "=" + ldap.EscapeSearch(id) + ")"
	}
	return ""
}
