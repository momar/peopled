package field

import (
	"codeberg.org/momar/peopled/api"
	"codeberg.org/momar/peopled/ldap"
	"fmt"
	"math/rand"
)

type Separator struct {
	Read []string
	Title api.TranslatedString
	i uint64
}

var _ ldap.Field = &Separator{}

func (field *Separator) Setup() error {
	return nil
}

func (field *Separator) New() ldap.Field {
	return &Separator{field.Read, field.Title, rand.Uint64()}
}

func (field *Separator) FromLDAP(in map[string][]string) error {
	return nil
}

func (field *Separator) ToLDAP() map[string][]string {
	return map[string][]string{}
}

func (field *Separator) FromJSON(in interface{}) error {
	return nil
}

func (field *Separator) ToJSON() interface{} {
	return nil
}

func (field *Separator) Schema() api.FieldSchema {
	return api.FieldSchema{
		Name: field.Name(),
		Type: "separator",
		Options: map[string]interface{}{
			"title": field.Title,
		},
		ReadViews: field.Read,
		WriteViews: []string{},
	}
}

func (field *Separator) Name() string {
	return "separator" + fmt.Sprint(field.i)
}

func (field *Separator) GetViews() api.FieldViews {
	return api.FieldViews{
		Read:   field.Read,
		Update: []string{},
	}
}
