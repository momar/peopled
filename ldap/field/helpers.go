package field

import "codeberg.org/momar/peopled/api"

type Required bool

// Attribute is a helper for single-attribute based fields.
type Attribute string

func (a Attribute) Name() string {
	return string(a)
}

type Views api.FieldViews

func (v Views) GetViews() api.FieldViews {
	return api.FieldViews(v)
}
