package field

import (
	"codeberg.org/momar/peopled/api"
	"codeberg.org/momar/peopled/ldap"
	"crypto/rand"
	"crypto/sha1"
	"encoding/base64"
	"fmt"
)

// Password is the main UID/Group CN field
type Password struct {
	Attribute
	Views
	Title        api.TranslatedString
	Icon         string
	//Complexity uint
	MinLength  uint
	value      string
}

var _ ldap.Field = &Password{}

func (field *Password) Setup() error {
	return nil
}

func (field *Password) New() ldap.Field {
	return &Password{
		field.Attribute,
		field.Views,
		field.Title,
		field.Icon,
		//field.Complexity,
		field.MinLength,
		"",
	}
}

func (field *Password) FromLDAP(in map[string][]string) error {
	if in[field.Name()] != nil && len(in[field.Name()]) > 0 {
		field.value = in[field.Name()][0]
	} else {
		field.value = ""
	}
	return nil
}

func (field *Password) ToLDAP() map[string][]string {
	return map[string][]string{string(field.Attribute): {field.value}}
}

func (field *Password) FromJSON(in interface{}) error {
	if password, ok := in.(string); !ok {
		return fmt.Errorf("must be a string")
	} else {
		if uint(len(password)) < field.MinLength {
			return fmt.Errorf("must be at least %d characters long", field.MinLength)
		}

		/*c := field.Complexity
		tables := [][]*unicode.RangeTable{
			{unicode.Digit},
			{unicode.Lower},
			{unicode.Upper},
			{unicode.Punct, unicode.Symbol},
		}
		strings.IndexFunc(password, func(r rune) bool {
			if len(tables) < 1 {
				c--
				return true
			}
			for i, table := range tables {
				if unicode.IsOneOf(table, r) {
					c--
					if c <= 0 {
						return true
					}
					tables = append(tables[:i], tables[i+1:]...)
				}
			}
			return false
		})
		if c <= 0 {
			return fmt.Errorf("must be at least have %d character classes", field.Complexity)
		}*/

		salt := make([]byte, 32)
		n, err := rand.Read(salt)
		if n < 32 || err != nil {
			return fmt.Errorf("couldn't generate salt (%w)", err)
		}
		hash := sha1.Sum(append([]byte(password), salt...))
		password = "{SSHA}" + base64.StdEncoding.EncodeToString(append(hash[:], salt...))
		field.value = password
		return nil
	}
}

func (field *Password) ToJSON() interface{} {
	return nil
}

func (field *Password) Schema() api.FieldSchema {
	return api.FieldSchema{
		Name: field.Name(),
		Type: "password",
		Options: map[string]interface{}{
			"title": field.Title,
			"icon": field.Icon,
			"minLength":  field.MinLength,
			//"complexity": field.Complexity,
		},
		ReadViews: field.Views.Read,
		WriteViews: field.Views.Update,
	}
}
