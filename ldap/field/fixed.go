package field

import (
	"codeberg.org/momar/peopled/api"
	"codeberg.org/momar/peopled/ldap"
	"fmt"
	"github.com/Masterminds/sprig"
	"strings"
	"text/template"
)

// Fixed is a field with a fixed (or templated) value. All fixed fields must be at the end of your configuration!
type Fixed struct {
	Attribute
	Views
	Value    string
	valueTpl *template.Template
	valueRes string
}

var _ ldap.Field = &Fixed{}

func (field *Fixed) Setup() error {
	if field.Value != "" {
		tpl, err := template.New("ldap-fixed-" + field.Name()).Funcs(sprig.TxtFuncMap()).Parse(field.Value)
		if err != nil {
			return fmt.Errorf("couldn't parse template: %w", err)
		}
		field.valueTpl = tpl
	} else {
		return fmt.Errorf("fixed field requires a template")
	}
	return nil
}

func (field *Fixed) New() ldap.Field {
	return &Fixed{
		field.Attribute,
		field.Views,
		field.Value,
		field.valueTpl,
		"",
	}
}

func (field *Fixed) FromLDAP(in map[string][]string) error {
	if value, ok := in[field.Name()]; ok && len(value) > 0 {
		field.valueRes = value[0]
	}
	return nil
}

func (field *Fixed) ApplyTemplates(context map[string]interface{}) error {
	value := &strings.Builder{}
	err := field.valueTpl.Execute(value, context)
	if err != nil {
		return fmt.Errorf("can't execute template (%w)", err.Error())
	}
	field.valueRes = value.String()
	context[field.Name()] = field.valueRes
	return nil
}

func (field *Fixed) ToLDAP() map[string][]string {
	if field.valueRes == "" {
		return map[string][]string{field.Name(): {}}
	}
	return map[string][]string{field.Name(): {field.valueRes}}
}

func (field *Fixed) FromJSON(in interface{}) error {
	return fmt.Errorf("field is read-only")
}

func (field *Fixed) ToJSON() interface{} {
	return field.valueRes
}

func (field *Fixed) Schema() api.FieldSchema {
	return api.FieldSchema{
		Name: field.Name(),
		Type: "fixed",
		ReadViews: field.Views.Read,
		WriteViews: []string{},
	}
}
