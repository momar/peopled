package field

import (
	"codeberg.org/momar/peopled/api"
	"codeberg.org/momar/peopled/ldap"
	"encoding/base64"
	"errors"
)

// Picture is an image file, e.g. for the user avatar
type Picture struct {
	Attribute
	Views
	Title api.TranslatedString
	MaxFilesize uint
	value       string
}

var _ ldap.Field = &Picture{}

func (field *Picture) Setup() error {
	return nil
}

func (field *Picture) New() ldap.Field {
	return &Picture{
		field.Attribute,
		field.Views,
		field.Title,
		field.MaxFilesize,
		"",
	}
}

func (field *Picture) FromLDAP(in map[string][]string) error {
	if value, ok := in[string(field.Attribute)]; !ok || len(value) < 1 {
		field.value = ""
	} else {
		field.value = "data:image/jpeg;base64," + base64.StdEncoding.EncodeToString([]byte(value[0]))
	}
	return nil
}

func (field *Picture) ToLDAP() map[string][]string {
	if len(field.value) < 1 {
		return map[string][]string{field.Name(): {}}
	}
	b, _ := base64.StdEncoding.DecodeString(field.value[23:])
	return map[string][]string{field.Name(): {string(b)}}
}

func (field *Picture) FromJSON(in interface{}) error {
	if uri, ok := in.(string); ok {
		field.value = uri
	} else {
		return errors.New("must be a string")
	}
	return nil
}

func (field *Picture) ToJSON() interface{} {
	return field.value
}

func (field *Picture) Schema() api.FieldSchema {
	return api.FieldSchema{
		Name: field.Name(),
		Type: "picture",
		Options: map[string]interface{}{
			"title": field.Title,
			"maxFilesize": field.MaxFilesize,
		},
		ReadViews: field.Views.Read,
		WriteViews: field.Views.Update,
	}
}
