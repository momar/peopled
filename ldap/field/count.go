package field

import (
	"codeberg.org/momar/peopled/api"
	"codeberg.org/momar/peopled/ldap"
	"fmt"
	"strconv"
)

// Count is a field that returns the number of resources of a specific type (plus an offset) if it's nil, and is built for the uidNumber field of LDAP.
type Count struct {
	Attribute
	Views
	Offset int
	Resource string
	value *int
}

var _ ldap.Field = &Count{}

func (field *Count) Setup() error {
	if _, ok := api.ActiveConfig.Resources[field.Resource]; !ok {
		return fmt.Errorf("resource type %s doesn't exist", field.Resource)
	}
	return nil
}

func (field *Count) New() ldap.Field {
	return &Count{
		field.Attribute,
		field.Views,
		field.Offset,
		field.Resource,
		nil,
	}
}

func (field *Count) FromLDAP(in map[string][]string) error {
	if valueStrings, ok := in[field.Name()]; ok && len(valueStrings) > 0 {
		if value, err := strconv.Atoi(valueStrings[0]); err == nil {
			field.value = &value
			return nil
		}
	}
	field.value = nil
	return nil
}

func (field *Count) ApplyTemplates(context map[string]interface{}) error {
	if field.value != nil {
		return nil
	}
	list, err := api.ActiveConfig.Resources[field.Resource].List(nil)
	if err != nil {
		return err
	}
	listLength := len(list) + field.Offset
	field.value = &listLength
	context[field.Name()] = strconv.Itoa(*field.value)
	return nil
}

func (field *Count) ToLDAP() map[string][]string {
	return map[string][]string{field.Name(): {strconv.Itoa(*field.value)}}
}

func (field *Count) FromJSON(in interface{}) error {
	return fmt.Errorf("field is read-only")
}

func (field *Count) ToJSON() interface{} {
	return field.value
}

func (field *Count) Schema() api.FieldSchema {
	return api.FieldSchema{
		Name: field.Name(),
		Type: "count",
		ReadViews: field.Views.Read,
		WriteViews: []string{},
	}
}
