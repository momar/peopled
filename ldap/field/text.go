package field

import (
	"codeberg.org/momar/peopled/api"
	"codeberg.org/momar/peopled/ldap"
	"fmt"
	"github.com/Masterminds/sprig"
	"regexp"
	"strings"
	"text/template"
)

// Text is a simple text field
type Text struct {
	Attribute
	Views
	Required
	Default      string // If this is non-empty an empty value will be overwritten with the template value.
	defaultTpl   *template.Template
	Identifier   bool
	Long         bool
	Multiple     bool
	Autocomplete string
	Spellcheck   bool
	Readonly     bool
	Title        api.TranslatedString
	Icon         string
	Choices      []string
	Type         string
	//Choices     map[string]string
	//Suggestions []string
	Validations []*regexp.Regexp
	value       []string
}

var _ ldap.Field = &Text{}

func (field *Text) Setup() error {
	// TODO: validate views & configuration
	if field.Default != "" {
		tpl, err := template.New("ldap-text-" + field.Name()).Funcs(sprig.TxtFuncMap()).Parse(field.Default)
		if err != nil {
			return fmt.Errorf("couldn't parse default template: %w", err)
		}
		field.defaultTpl = tpl
	}
	if field.Choices != nil && len(field.Choices) > 0 {
		field.Type = "select"
	} else if field.Long {
		field.Type = "textarea"
	}
	return nil
}

func (field *Text) New() ldap.Field {
	return &Text{
		field.Attribute,
		field.Views,
		field.Required,
		field.Default,
		field.defaultTpl,
		field.Identifier,
		field.Long,
		field.Multiple,
		field.Autocomplete,
		field.Spellcheck,
		field.Readonly,
		field.Title,
		field.Icon,
		field.Choices[:],
		field.Type,
		field.Validations[:],
		[]string{},
	}
}

func (field *Text) FromLDAP(in map[string][]string) error {
	if value, ok := in[field.Name()]; !ok {
		field.value = []string{}
	} else {
		field.value = value
	}
	return nil
}

func (field *Text) ToLDAP() map[string][]string {
	for i := 0; i < len(field.value); i++ {
		if field.value[i] == "" {
			field.value = append(field.value[:i], field.value[i+1:]...)
			i--
		}
	}
	return map[string][]string{field.Name(): field.value}
}

func (field *Text) ApplyTemplates(context map[string]interface{}) error {
	if (context[field.Name()] == nil || context[field.Name()] == "") && field.defaultTpl != nil {
		value := &strings.Builder{}
		err := field.defaultTpl.Execute(value, context)
		if err != nil {
			return fmt.Errorf("can't execute default template (%w)", err)
		}
		field.value = []string{value.String()}
	}
	context[field.Name()] = field.value
	return nil
}

func (field *Text) FromJSON(in interface{}) error {
	if field.Multiple {
		// TODO: NYI
	}

	if value, ok := in.(string); !ok {
		return fmt.Errorf("must be a string")
	} else {
		if field.Validations != nil {
			for i, v := range field.Validations {
				if !v.MatchString(value) {
					return fmt.Errorf("value doesn't match Validations[%d]", i)
				}
			}
		}
		field.value = []string{value}
		return nil
	}
}

func (field *Text) ToJSON() interface{} {
	var v interface{} = field.value
	if !field.Multiple {
		if len(field.value) < 1 {
			v = ""
		} else {
			v = field.value[0]
		}
	}
	return v
}

func (field *Text) Schema() api.FieldSchema {
	return api.FieldSchema{
		Name: field.Name(),
		Type: "text",
		Options: map[string]interface{}{
			"type":         field.Type,
			"long":         field.Long,
			"multiple":     field.Multiple,
			"autocomplete": field.Autocomplete,
			"spellcheck":   field.Spellcheck,
			"readonly":     field.Readonly,
			"title":        field.Title,
			"icon":         field.Icon,
			"required":     field.Required,
			"choices":      field.Choices,
		},
		ReadViews: field.Views.Read,
		WriteViews: field.Views.Update,
	}
}

func (field *Text) Identify(id string) string {
	if field.Identifier {
		return "(" + field.Name() + "=" + ldap.EscapeSearch(id) + ")"
	}
	return ""
}
