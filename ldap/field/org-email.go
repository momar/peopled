package field

import (
	"codeberg.org/momar/peopled/ldap"
	"fmt"
	"github.com/Masterminds/sprig"
	"strings"
	"text/template"
)

// OrgEmail is an email field, but it replaces all addresses on an organization's domain with a fixed set of addresses from a template.
//
// Example usage:
//
// ```go
// &OrgEmail{Email{...}, map[string][]string{
//   "example.org": []string{
//     `{{ .uid }}`, // jdoe@example.org
//     `{{ .givenName }}.{{ .sn }}`, // jane.doe@example.org
//   },
// }
// ```
type OrgEmail struct {
	Email
	DomainTemplates map[string][]string
	domainTemplates map[string][]*template.Template
}

var _ ldap.UpdatableField = &OrgEmail{}

func (field *OrgEmail) Setup() error {
	tpls := map[string][]*template.Template{}
	for domain, tplStrings := range field.DomainTemplates {
		tpls[domain] = make([]*template.Template, len(tplStrings))
		for i, tplString := range tplStrings {
			tpl, err := template.New("ldap-orgemail-" + field.Name()).Funcs(sprig.TxtFuncMap()).Parse(tplString)
			if err != nil {
				return fmt.Errorf("can't parse template %d for domain %s (server configuration issue: %s)", field.Name(), i, domain, err.Error())
			}
			tpls[domain][i] = tpl
		}
	}
	field.domainTemplates = tpls
	return nil
}

func (field *OrgEmail) New() ldap.Field {
	return &OrgEmail{
		Email{
			field.Attribute,
			field.Views,
			field.Required,
			field.Identifier,
			field.Title,
			field.Icon,
			field.MaxCount,
			field.MinCount,
			//class.RequireValidation,
			[]string{},
		},
		field.DomainTemplates,
		field.domainTemplates,
	}
}

func (field *OrgEmail) removeOrgDomains(emails []string) []string {
	for i := 0; i < len(emails); i++ {
		for domain, _ := range field.DomainTemplates {
			if strings.HasSuffix(emails[i], "@"+domain) {
				emails = append(emails[:i], emails[i+1:]...)
				i--
				break
			}
		}
	}
	return emails
}

func (field *OrgEmail) FromJSON(in interface{}) error {
	emails, err := field.parseJSON(in)
	if err != nil {
		return err
	}
	emails = field.removeOrgDomains(emails)
	return field.applyJSON(emails)
}

func (field *OrgEmail) ApplyTemplates(context map[string]interface{}) error {
	field.value = field.removeOrgDomains(field.value)

	for domain, tpls := range field.domainTemplates {
		for i, tpl := range tpls {
			address := &strings.Builder{}
			err := tpl.Execute(address, context)
			if err != nil {
				return fmt.Errorf("can't execute template %d for domain %s (%w)", i, domain, err)
			}
			field.value = append(field.value, address.String())
		}
	}

	context[field.Name()] = field.value
	return nil
}
