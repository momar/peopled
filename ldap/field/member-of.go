package field

import (
	"codeberg.org/momar/peopled/api"
	"codeberg.org/momar/peopled/ldap"
	"errors"
	"fmt"
	"strings"
)

// MemberOf is an editable list of groups a user is in. Modifications get handled by user-write.go.
type MemberOf struct {
	Attribute
	Views
	DNTemplate string
	value      []string
}

// TODO: allow modifications!

var _ ldap.Field = &MemberOf{}

func (field *MemberOf) Setup() error {
	if field.DNTemplate == "" || !strings.Contains(field.DNTemplate, "%s") {
		return errors.New("DNTemplate is required & must contain a placeholder (%s)")
	}
	return nil
}

func (field *MemberOf) New() ldap.Field {
	return &MemberOf{
		field.Attribute,
		field.Views,
		field.DNTemplate,
		[]string{},
	}
}

func (field *MemberOf) FromLDAP(in map[string][]string) error {
	if groups, ok := in[field.Name()]; !ok {
		field.value = []string{}
	} else {
		field.value = groups
	}
	return nil
}

func (field *MemberOf) ToLDAP() map[string][]string {
	return map[string][]string{}
}

func (field *MemberOf) FromJSON(in interface{}) error {
	groups, ok := in.([]string)
	if !ok {
		if groupsInterface, ok := in.([]interface{}); ok {
			groups = make([]string, len(groupsInterface))
			for i, groupInterface := range groupsInterface {
				if group, ok := groupInterface.(string); ok {
					groups[i] = group
				} else {
					return fmt.Errorf("must be a string array")
				}
			}
		} else {
			return fmt.Errorf("must be a string array")
		}
	}
	for i := 0; i < len(groups); i++ {
		groups[i] = field.FormatDN(groups[i])
	}
	field.value = groups
	return nil
}

func (field *MemberOf) ToJSON() interface{} {
	//log.Debug().Strs("memberOf", field.value).Msg("out")
	value := field.value[:]
	for i := 0; i < len(value); i++ {
		value[i] = field.UnwrapDN(value[i])
	}
	return field.value
}

func (field *MemberOf) Schema() api.FieldSchema {
	return api.FieldSchema{
		Name: field.Name(),
		Type: "memberOf",
		ReadViews: field.Views.Read,
		WriteViews: field.Views.Update,
	}
}

func (field *MemberOf) FormatDN(id string) string {
	return strings.Replace(field.DNTemplate, "%s", ldap.EscapeDN(id), 1)
}
func (field *MemberOf) UnwrapDN(dn string) string {
	index := strings.Index(field.DNTemplate, "%s")
	return ldap.UnescapeDN(strings.TrimSuffix(strings.TrimPrefix(dn, field.DNTemplate[:index]), field.DNTemplate[index+2:]))
}
func (field *MemberOf) MatchDN(dn string) bool {
	index := strings.Index(field.DNTemplate, "%s")
	return len(dn) >= len(field.DNTemplate)-1 && strings.HasPrefix(dn, field.DNTemplate[:index]) && strings.HasSuffix(dn, field.DNTemplate[index+2:])
}
