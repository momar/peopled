package api

import (
	"github.com/gin-gonic/gin"
)

// resourceList (GET /resource/:type)
func resourceList(c *gin.Context) {
	// TODO: query & pagination?!
	resType, ok := ActiveConfig.Resources[c.Param("type")]
	if !ok {
		returnError(c, ErrResourceTypeDoesntExist)
		return
	}

	user, viewpoint := GetViewpointByContext(c)
	if err := resType.ListCheck(viewpoint); err != nil {
		if user == nil {
			returnError(c, ErrAuthenticationRequired)
			return
		}
		returnError(c, err)
		return
	}

	ids, err := resType.List(Query{})
	if err != nil {
		returnError(c, err)
		return
	}
	c.JSON(200, ids)
}

// resourceGet (GET /resource/:type/:id)
func resourceGet(c *gin.Context) {
	resType, ok := ActiveConfig.Resources[c.Param("type")]
	if !ok {
		returnError(c, ErrResourceTypeDoesntExist)
		return
	}

	user, viewpoint := GetViewpointByContext(c)
	if c.Param("type") == ActiveConfig.UserResource && user.UID == c.Param("id") {
		viewpoint["selfservice"] = struct{}{}
	}

	res := resType.New()
	err := res.Read(c.Param("id"))
	if err != nil {
		returnError(c, err)
		return
	}

	body, err := res.ToBody(viewpoint)
	if err != nil {
		returnError(c, err)
		return
	}
	c.JSON(200, body)
}
