package api

import (
	"codeberg.org/momar/peopled/api/kv"
	"codeberg.org/momar/peopled/api/kv/bolt"
	"codeberg.org/momar/peopled/api/kv/memory"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/jordan-wright/email"
	"github.com/rs/zerolog/log"
	"image/color"
	"os"
	"strings"
	"time"

	"codeberg.org/momar/linkding/names"
	"github.com/gin-gonic/gin"
	"github.com/mojocn/base64Captcha"
)

// TODO: Unit Tests
// TODO: Documentation

type User struct {
	UID      string   `json:"username"`
	MemberOf []string `json:"memberOf"`
	Name     string   `json:"name"`
	Email    []string `json:"email"`
}

type Signup struct {
	Username string          `json:"username"`
	Password string          `json:"password"`
	Name     string          `json:"name"`
	Email    string          `json:"email"`
	Accept   map[string]bool `json:"accept"`
	Language string
	Captcha struct{
		ID string `json:"id"`
		Response string `json:"response"`
	} `json:"captcha"`
}

////////////////////////////
// LOGIN, LOGOUT & SIGNUP //
////////////////////////////

// doCheckUsername (GET /user/:username)
func doCheckUsername(c *gin.Context) {
	user := GetUserByName(c.Param("username"))
	if user == nil {
		c.JSON(200, gin.H{"exists": false})
	} else {
		c.JSON(200, gin.H{"exists": true})
	}
}

var captcha = base64Captcha.NewCaptcha((&base64Captcha.DriverString{
	Height:          80,
	Width:           275,
	NoiseCount:      35,
	ShowLineOptions: base64Captcha.OptionShowSlimeLine,
	Length:          6,
	Source:          "234689abcdefghkmnpqrtuvwxyz",
	BgColor:         &color.RGBA{255, 255, 255, 0},
	Fonts:           []string{
		"ApothecaryFont.ttf",
		"DENNEthree-dee.ttf",
		"Flim-Flam.ttf",
		"RitaSmith.ttf",
		"actionj.ttf",
		"chromohv.ttf",
	},
}).ConvertFonts(), base64Captcha.DefaultMemStore)

// doGetCaptcha (GET /signup)
func doGetCaptcha(c *gin.Context) {
	if !ActiveConfig.EnableSignupCaptcha {
		returnError(c, ErrCaptchaIsDisabled)
		return
	}

	id, data, err := captcha.Generate()
	if err != nil {
		returnError(c, err)
		return
	}
	c.JSON(200, gin.H{"id": id, "data": data})
}

// doCreateSignup (POST /signup)
func doCreateSignup(c *gin.Context) {
	if !ActiveConfig.EnableSignup {
		returnError(c, ErrSignupsAreDisabled)
		return
	}

	req := Signup{}
	err := c.BindJSON(&req)
	if err != nil {
		returnError(c, ErrUnparsableRequestBody)
		return
	}

	authenticationUserResource, err := getAuthenticationUserResource()
	if err != nil {
		returnError(c, err)
		return
	}

	// Validate & check for RequireAccept
	errs := authenticationUserResource.Signup(req, true)
	if errs == nil {
		errs = map[string]error{}
	}
	if ActiveConfig.RequireAccept != nil {
		for _, accept := range ActiveConfig.RequireAccept {
			if req.Accept[accept] == false {
				errs["accept."+accept] = errors.New("this field is mandatory")
			}
		}
	}
	if ActiveConfig.EnableSignupCaptcha {
		if req.Captcha.ID == "" || req.Captcha.Response == "" || !captcha.Verify(req.Captcha.ID, strings.ToLower(req.Captcha.Response), true) {
			errs["captcha.response"] = errors.New("captcha is invalid")
		}
	}
	if len(errs) > 0 {
		returnErrors(c, errs)
		return
	}

	// check if the user exists
	_, err = authenticationUserResource.Authenticate(req.Username, "")
	if err != ErrUserDoesntExist {
		returnError(c, ErrUsernameAlreadyExists)
		return
	}
	_, err = authenticationUserResource.Authenticate(req.Email, "")
	if err != ErrUserDoesntExist {
		returnError(c, ErrEmailAlreadyExists)
		return
	}

	req.Language = c.GetHeader("Accept-Language")

	if ActiveConfig.RequireEmailConfirmation {
		// email configuration is required
		data, err := json.Marshal(req)
		if err != nil {
			returnError(c, fmt.Errorf("couldn't marshal signup request", err))
			return
		}

		token := names.Generate("#x{64}")
		if token == "" {
			returnError(c, errors.New("couldn't generate bearer token"))
			return
		}

		cont, err := getTemplate("signup", c.GetHeader("Accept-Language"), map[string]interface{}{
			"link": strings.TrimSuffix(ActiveConfig.CanonicalRootURL, "/") + "/#/signup/" + token,
		})
		if err != nil {
			returnError(c, fmt.Errorf("Mailtemplate error: %s", err))
			return
		}

		mail := &email.Email{
			To:      []string{req.Email},
			Subject: getSubject("signup", c.GetHeader("Accept-Language")),
			Text:    cont,
		}
		err = ActiveConfig.EmailConfig.Send(mail)
		if err != nil {
			returnError(c, fmt.Errorf("couldn't send email: %w", err))
			return
		}

		err = store.Set("signup", token, string(data), 6*time.Hour)
		if err != nil {
			returnError(c, fmt.Errorf("couldn't store signup token: %w", err))
			return
		}

		c.JSON(200, gin.H{"success": true})
	} else if ActiveConfig.RequireAdminConfirmation {
		// admin confirmation is required (but not email confirmation)
		err := sendAdminConfirmation(req)
		if err != nil {
			returnError(c, err)
			return
		}
		c.JSON(200, gin.H{
			"success": true,
		})
	} else {
		// no confirmation required, sign up the user directly
		errs = authenticationUserResource.Signup(req, false)
		if errs != nil {
			returnErrors(c, errs)
			return
		}
		c.JSON(200, gin.H{
			"success": true,
		})
	}
}

func sendAdminConfirmation(req Signup) error {
	data, err := json.Marshal(req)
	if err != nil {
		return fmt.Errorf("couldn't marshal signup request: %w", err)
	}

	token := names.Generate("#x{64}")
	if token == "" {
		return errors.New("couldn't generate bearer token")
	}

	recipients := []string{}
	if ActiveConfig.AdminConfirmationEmails != nil {
		recipients = ActiveConfig.AdminConfirmationEmails[:]
	}
	if ActiveConfig.AdminConfirmationGroups != nil && len(ActiveConfig.AdminConfirmationGroups) > 0 {
		usernames, err := ActiveConfig.Resources[ActiveConfig.UserResource].List(nil) // TODO: use a query
		if err != nil {
			return fmt.Errorf("couldn't read usernames: %w", err)
		}
		for _, username := range usernames {
			user := GetUserByName(username)
			if user == nil {
				return errors.New("user " + username + " doesn't exist")
			}
			for _, userGroup := range user.MemberOf {
				for _, group := range ActiveConfig.AdminConfirmationGroups {
					if userGroup == group {
						recipients = append(recipients, user.Email...)
					}
				}
			}
		}
	}
	if len(recipients) == 0 {
		return errors.New("no administrators found")
	}
	log.Trace().Strs("recipients", recipients).Msg("sending admin confirmation")

	subj := getSubject("admin-confirmation", ActiveConfig.AdminConfirmationLanguage)
	cont, err := getTemplate("admin-confirmation", ActiveConfig.AdminConfirmationLanguage, map[string]interface{}{
		"link": strings.TrimSuffix(ActiveConfig.CanonicalRootURL, "/") + "/#/signup/" + token,
		"email": req.Email,
		"username": req.Username,
		"name": req.Name,
		"accept": req.Accept,
	})
	if err != nil {
		return fmt.Errorf("Mailtemplate error: %s", err)
	}

	for _, recipient := range recipients {
		mail := &email.Email{
			To:      []string{recipient},
			Subject: subj,
			Text:    cont,
		}
		err = ActiveConfig.EmailConfig.Send(mail)
		if err != nil {
			return fmt.Errorf("couldn't send email: %w", err)
		}
	}

	err = store.Set("admin-confirmation", token, string(data), 14*24*time.Hour)
	if err != nil {
		return fmt.Errorf("couldn't store admin confirmation token: %w", err)
	}
	return nil
}

// doCreateAuthentication (POST /authentication)
func doCreateAuthentication(c *gin.Context) {
	// Parse the POST body
	req := struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}{}
	err := c.BindJSON(&req)
	if err != nil || req.Username == "" || req.Password == "" {
		returnError(c, ErrUsernameOrPasswordMissing)
		return
	}

	// Authenticate the user against the backend
	authenticationUserResource, err := getAuthenticationUserResource()
	if err != nil {
		returnError(c, err)
		return
	}

	user, err := authenticationUserResource.Authenticate(req.Username, req.Password)
	if err != nil {
		returnError(c, err)
		return
	}

	// Generate & store login token
	token := names.Generate("#x{64}")
	if token == "" {
		returnError(c, errors.New("couldn't generate bearer token"))
		return
	}
	err = store.Set("login", token, user.UID, 7*24*time.Hour)
	if err != nil {
		returnError(c, fmt.Errorf("couldn't store login token: %w", err))
		return
	}

	// Return the token
	c.JSON(200, gin.H{"token": token})
}

// doRetrieveCurrentUser (GET /user)
func doRetrieveCurrentUser(c *gin.Context) {
	user := GetUserByContext(c)
	if user == nil {
		returnError(c, ErrAuthenticationRequired)
		return
	}
	c.JSON(200, user)
}

// doDeleteAuthentication (DELETE /authentication)
func doDeleteAuthentication(c *gin.Context) {
	uid, token := getToken(c, "login")
	if uid == "" {
		returnError(c, ErrAuthenticationRequired)
		return
	}
	err := store.Del("login", token)
	if err != nil {
		returnError(c, fmt.Errorf("couldn't delete login token: %w", err))
		return
	}
	c.JSON(200, gin.H{"success": true})
}

/////////////////////////////
// RECOVERY & CONFIRMATION //
/////////////////////////////

// doCreateRecovery (POST /recovery)
func doCreateRecovery(c *gin.Context) {
	if !ActiveConfig.EnableRecovery {
		returnError(c, ErrRecoveryIsDisabled)
		return
	}

	uid := struct{ Username string `json:"username"` }{}
	err := c.BindJSON(&uid)
	if err != nil {
		returnError(c, ErrUnparsableRequestBody)
		return
	}

	user := GetUserByName(uid.Username)
	if user == nil {
		returnError(c, ErrUserDoesntExist)
		return
	}

	token := names.Generate("#x{64}")
	if token == "" {
		returnError(c, errors.New("couldn't generate bearer token"))
		return
	}

	cont, err := getTemplate("recovery", c.GetHeader("Accept-Language"), map[string]interface{}{
		"link":strings.TrimSuffix(ActiveConfig.CanonicalRootURL, "/") + "/#/recovery/" + token,
	})
	if err != nil {
		returnError(c, fmt.Errorf("Mailtemplate error: %s", err))
		return
	}

	mail := &email.Email{
		To:      user.Email,
		Subject: getSubject("recovery", c.GetHeader("Accept-Language")),
		Text:    cont,
	}
	err = ActiveConfig.EmailConfig.Send(mail)
	if err != nil {
		returnError(c, fmt.Errorf("couldn't send email: %w", err))
		return
	}

	err = store.Set("recovery", token, uid.Username, 6*time.Hour)
	if err != nil {
		returnError(c, fmt.Errorf("couldn't store recovery token: %w", err))
		return
	}
	c.JSON(200, gin.H{"success": true})
}

// doConfirmRecovery (POST /recovery/:token)
func doConfirmRecovery(c *gin.Context) {
	uid, token := getToken(c, "recovery")
	if uid == "" {
		returnError(c, ErrInvalidRecoveryToken)
		return
	}

	user := GetUserByName(uid)
	if user == nil {
		returnError(c, ErrUserDoesntExist)
		return
	}

	req := struct {
		Password string `json:"password"`
	}{}
	err := c.BindJSON(&req)
	if err != nil {
		returnError(c, ErrUnparsableRequestBody)
		return
	}

	authenticationUserResource, err := getAuthenticationUserResource()
	if err != nil {
		returnError(c, err)
		return
	}

	err = authenticationUserResource.SetPassword(user.UID, req.Password)
	if err != nil {
		returnError(c, err)
		return
	}

	_ = store.Del("recovery", token)
	c.JSON(200, gin.H{"success": true})
}

// doConfirmSignup (POST /signup/:token)
func doConfirmSignup(c *gin.Context) {
	reqData, token := getToken(c, "signup")
	isAdminConfirmation := false
	if reqData == "" {
		reqData, token = getToken(c, "admin-confirmation")
		isAdminConfirmation = true
		if reqData == "" {
			returnError(c, ErrInvalidConfirmationToken)
			return
		}
	}
	req := Signup{}
	if err := json.Unmarshal([]byte(reqData), &req); err != nil {
		returnError(c, fmt.Errorf("couldn't parse signup request: %w", err))
		return
	}

	if ActiveConfig.RequireAdminConfirmation && !isAdminConfirmation {
		// admin confirmation is required
		err := sendAdminConfirmation(req)
		if err != nil {
			returnError(c, err)
			return
		}
	} else {
		authenticationUserResource, err := getAuthenticationUserResource()
		if err != nil {
			returnError(c, err)
			return
		}

		errs := authenticationUserResource.Signup(req, false)
		if errs != nil {
			returnErrors(c, errs)
			return
		}
	}

	if isAdminConfirmation {
		_ = store.Del("admin-confirmation", token)
		c.JSON(200, gin.H{"success": true, "isAdminConfirmation": true})

		cont, err := getTemplate("signup-confirmed-by-admin", req.Language, map[string]interface{}{
			"link": strings.TrimSuffix(ActiveConfig.CanonicalRootURL, "/") + "/",
		})
		if err != nil {
			returnError(c, fmt.Errorf("Mailtemplate error: %s", err))
			return
		}

		mail := &email.Email{
			To:      []string{req.Email},
			Subject: getSubject("signup-confirmed-by-admin", req.Language),
			Text:    cont,
		}
		err = ActiveConfig.EmailConfig.Send(mail)
		if err != nil {
			returnError(c, fmt.Errorf("couldn't send email: %w", err))
			return
		}
	} else {
		_ = store.Del("signup", token)
		c.JSON(200, gin.H{"success": true})
	}
}

/////////////
// Helpers //
/////////////

var store kv.Store = memory.New()

func init() {
	if p := os.Getenv("TOKEN_DATABASE"); p != "" {
		var err error
		store, err = bolt.New(p)
		if err != nil {
			panic(err)
		}
	}
}

func getToken(c *gin.Context, bucket string) (uid string, token string) {
	if tokenParam, ok := c.Params.Get("token"); ok {
		token = tokenParam
	} else if tokenHeader := strings.TrimPrefix(c.GetHeader("Authorization"), "Bearer "); tokenHeader != "" {
		token = tokenHeader
	} else if tokenQuery := c.Query("token"); tokenQuery != "" {
		token = tokenQuery
	} else {
		return "", ""
	}
	uid, deadline := store.Get(bucket, token)
	// Renew login tokens once a day (deadline is 7 days after the last activity)
	if uid != "" && bucket == "login" && deadline.Sub(time.Now()).Hours() < 6*24 {
		_ = store.Set(bucket, token, uid, 7*24*time.Hour)
	}
	return uid, token
}

func GetUserByContext(c *gin.Context) *User {
	// Get the uid from the token for this request
	uid, _ := getToken(c, "login")
	if uid == "" {
		return nil
	}

	// Return the corresponding user object from the core API
	return GetUserByName(uid)
}

func GetUserByName(uid string) *User {
	userResource, ok := ActiveConfig.Resources[ActiveConfig.UserResource]
	if !ok {
		return nil
	}
	authenticationUserResource, ok := userResource.(AuthenticationResourceType)
	if !ok {
		return nil
	}

	user, err := authenticationUserResource.Authenticate(uid, "")
	if err != nil {
		return nil
	}

	return &user
}

func getAuthenticationUserResource() (AuthenticationResourceType, error) {
	userResource, ok := ActiveConfig.Resources[ActiveConfig.UserResource]
	if !ok {
		return nil, errors.New("UserResource is not a valid resource type")
	}
	authenticationUserResource, ok := userResource.(AuthenticationResourceType)
	if !ok {
		return nil, errors.New("UserResource doesn't implement authentication")
	}
	return authenticationUserResource, nil
}
