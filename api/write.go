package api

import (
	"github.com/gin-gonic/gin"
)

// resourceCreate (POST /resource/:type)
func resourceCreate(c *gin.Context) {
	resType, ok := ActiveConfig.Resources[c.Param("type")]
	if !ok {
		returnError(c, ErrResourceTypeDoesntExist)
		return
	}

	user, viewpoint := GetViewpointByContext(c)
	if c.Param("type") == ActiveConfig.UserResource && user.UID == c.Param("id") {
		viewpoint["selfservice"] = struct{}{}
	}

	res := resType.New()

	var body = map[string]interface{}{}
	err := c.BindJSON(&body)
	if err != nil {
		returnError(c, ErrUnparsableRequestBody)
		return
	}

	errs := res.FromBody(body, viewpoint)
	if errs != nil {
		returnErrors(c, errs)
		return
	}

	err = res.Create(c.Param("id"))
	if err != nil {
		returnError(c, err)
		return
	}
	c.JSON(200, gin.H{"success": true})
}

// resourceUpdate (PUT /resource/:type/:id)
func resourceUpdate(c *gin.Context) {
	resType, ok := ActiveConfig.Resources[c.Param("type")]
	if !ok {
		returnError(c, ErrResourceTypeDoesntExist)
		return
	}

	user, viewpoint := GetViewpointByContext(c)
	if c.Param("type") == ActiveConfig.UserResource && user.UID == c.Param("id") {
		viewpoint["selfservice"] = struct{}{}
	}

	res := resType.New()
	err := res.Read(c.Param("id"))
	if err != nil {
		returnError(c, err)
		return
	}

	var body = map[string]interface{}{}
	err = c.BindJSON(&body)
	if err != nil {
		returnError(c, ErrUnparsableRequestBody)
		return
	}

	errs := res.FromBody(body, viewpoint)
	if errs != nil {
		returnErrors(c, errs)
		return
	}

	err = res.Update(c.Param("id"))
	if err != nil {
		returnError(c, err)
		return
	}
	c.JSON(200, gin.H{"success": true})
}

// resourceDelete (DELETE /resource/:type/:id)
func resourceDelete(c *gin.Context) {
	resType, ok := ActiveConfig.Resources[c.Param("type")]
	if !ok {
		returnError(c, ErrResourceTypeDoesntExist)
		return
	}

	user, viewpoint := GetViewpointByContext(c)
	if c.Param("type") == ActiveConfig.UserResource && user.UID == c.Param("id") {
		viewpoint["selfservice"] = struct{}{}
	}

	res := resType.New()
	err := res.Delete(c.Param("id"), viewpoint)
	if err != nil {
		returnError(c, err)
		return
	}
	c.JSON(200, gin.H{"success": true})
}
