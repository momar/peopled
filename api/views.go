package api

import (
	"github.com/gin-gonic/gin"
	"strings"
)

// ResourceViews is a list of allowed views for various request types.
type ResourceViews struct {
	Create []string // List of views that are sufficient to create a resource of this type.
	List   []string // List of views that are sufficient to list resources of this type.
	Delete []string // List of views that are sufficient to delete a resource of this type.
}

// ResourceViews is a list of allowed views for various request types.
type FieldViews struct {
	Read   []string // List of views that are sufficient to read a resource of this type.
	Update []string // List of views that are sufficient to update a resource of this type.
}

// Viewpoint is the list of visible views during a request.
type Viewpoint map[string]struct{}

func (ctx Viewpoint) Check(allowedViews []string) bool {
	if ctx == nil {
		return true // yes, ctx might be nil here!
	}
	if allowedViews == nil {
		return false
	}
	for _, allowedView := range allowedViews {
		requiredViews := strings.Split(allowedView, "+")
		allowed := len(requiredViews) > 0
		for _, requiredView := range requiredViews {
			if _, ok := ctx[requiredView]; !ok {
				allowed = false
				break
			}
		}
		if allowed {
			return true
		}
	}
	return false
}

func GetViewpointByContext(c *gin.Context) (*User, Viewpoint) {
	user := GetUserByContext(c)
	if user == nil {
		return nil, Viewpoint{}
	}

	viewpoint := Viewpoint{}
	viewpoint["user"] = struct{}{}
	viewpoint["user:"+user.UID] = struct{}{}
	for _, group := range user.MemberOf {
		viewpoint["group:"+group] = struct{}{}
	}
	return user, viewpoint
}
