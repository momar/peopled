package api

import (
	"bytes"
	"golang.org/x/text/language"
	"os"
	"path/filepath"
	"text/template"
)

func getTemplate(name string, langHeader string, data map[string]interface{}) ([]byte, error) {
	t, _ := language.MatchStrings(matcher, langHeader)
	base, _ := t.Base()
	lang := base.String()

	var tmpl *template.Template
	var err error
	if templatePath, ok := os.LookupEnv("TEMPLATE_PATH"); ok {
		tmpl, err = template.ParseFiles(filepath.Join(templatePath, lang, name + ".text"))
	} else {
		tmpl, err = template.New("Temp").Parse(fallback[lang][name])
	}
	if err != nil {
		return nil, err
	}

	var buf bytes.Buffer
	err = tmpl.Execute(&buf, data)
	if err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

func getSubject(name string, langHeader string) string {
	t, _ := language.MatchStrings(matcher, langHeader)
	base, _ := t.Base()
	return subject[base.String()][name]
}

var matcher = language.NewMatcher([]language.Tag{
	language.English,
	language.German,
})

var fallback = map[string]map[string]string{
	"en": {
		"signup": "To activate your account please click on the following link within the next 6 hours:\n\n{{index . \"link\"}}\n",
		"recovery": "Someone (probably yourself) requested a password reset. To set a new password click the link below within the next 6 hours:\n\n{{index . \"link\"}}\n",
		"admin-confirmation": "Please approve the new account for {{index . \"username\"}} by clicking the following link (valid for 14 days):\n\n{{index . \"link\"}}\n\nDetails of the new account:\n\n        Username: {{index . \"uid\"}}\n           Email: {{index . \"email\"}}\n            Name: {{index . \"name\"}}\n  Accepted terms: {{index . \"accept\"}}\n\nIf the token is invalid, another admin probably approved the account already.\n",
		"signup-confirmed-by-admin": "Your new account has been approved - you may now log in at {{index . \"link\"}}.\n",
	},
	"de": {
		"signup": "Um deinen Account zu aktivieren klicke bitte innerhalb der nächsten 6 Stunden auf den folgenden Link:\n\n{{index . \"link\"}}",
		"recovery": "Jemand (vermutlich du selbst) hat ein neues Passwort angefordert. Um ein neues Passwort zu vergeben, klicke bitte innerhalb der nächsten 6 Stunden auf den folgenden Link:\n\n{{index . \"link\"}}",
		"admin-confirmation": "Bitte bestätige den neuen Account für {{index . \"username\"}} über folgenden Link (14 Tage gültig):\n\n{{index . \"link\"}}\n\nAccount-Details:\n\n  Benutzername: {{index . \"uid\"}}\n        E-Mail: {{index . \"email\"}}\n          Name: {{index . \"name\"}}\n    Akzeptiert: {{index . \"accept\"}}\n\nWenn der Token ungültig ist wurde der Account vermutlich bereits von einem anderen Administrator bestätigt.\n",
		"signup-confirmed-by-admin": "Dein neuer Account wurde freigeschaltet - du kannst dich nun auf {{index . \"link\"}} anmelden.\n",
	},
}

var subject = map[string]map[string]string{
	"en": {
		"signup": "Please confirm your signup",
		"recovery": "Recover your account password",
		"admin-confirmation": "Please confirm a newly created account as an admin",
		"signup-confirmed-by-admin": "Your new account has been approved",
	},
	"de": {
		"signup": "Bitte bestätige deine Registrierung",
		"recovery": "Passwort zurücksetzen",
		"admin-confirmation": "Bitte bestätige einen neu erstellten Account als Administrator",
		"signup-confirmed-by-admin": "Dein neuer Account wurde freigeschaltet",
	},
}