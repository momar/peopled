package api

import (
	"github.com/gin-gonic/gin"
)

func schema(c *gin.Context) {
	user, viewpoint := GetViewpointByContext(c)
	schema := SchemaPublic{
		CanonicalRootURL:         ActiveConfig.CanonicalRootURL,
		EnableRecovery:           ActiveConfig.EnableRecovery,
		EnableSignup:             ActiveConfig.EnableSignup,
		EnableSignupCaptcha:      ActiveConfig.EnableSignupCaptcha,
		RequireAccept:            ActiveConfig.RequireAccept,
		RequireAdminConfirmation: ActiveConfig.RequireAdminConfirmation,
		Links:                    ActiveConfig.Links,
		RequireEmailConfirmation: ActiveConfig.RequireEmailConfirmation,
	}
	if user == nil {
		c.JSON(200, schema)
	} else {
		vp := make([]string, len(viewpoint))
		i := 0
		for x := range viewpoint {
			vp[i] = x
			i++
		}

		schema := SchemaPrivate{
			SchemaPublic: schema,
			UserResource: ActiveConfig.UserResource,
			Apps:         []App{},
			Tabs:         []Tab{},
			Resources:    map[string][]FieldSchema{},
			Viewpoints:   vp,
		}
		// add resources
		for name, resourceType := range ActiveConfig.Resources {
			schema.Resources[name] = resourceType.Schema()
		}
		// filter apps & tabs according to viewpoints
		for i := 0; i < len(ActiveConfig.Apps); i++ {
			if ActiveConfig.Apps[i].Views == nil || viewpoint.Check(ActiveConfig.Apps[i].Views) {
				schema.Apps = append(schema.Apps, ActiveConfig.Apps[i])
			}
		}
		for i := 0; i < len(ActiveConfig.Tabs); i++ {
			if ActiveConfig.Tabs[i].Views == nil || viewpoint.Check(ActiveConfig.Tabs[i].Views) {
				schema.Tabs = append(schema.Tabs, ActiveConfig.Tabs[i])
			}
		}
		c.JSON(200, schema)
	}
}

type SchemaPublic struct {
	CanonicalRootURL         string            `json:"canonicalRootURL"`
	EnableRecovery           bool              `json:"enableRecovery"`
	EnableSignup             bool              `json:"enableSignup"`
	EnableSignupCaptcha      bool              `json:"enableSignupCaptcha"`
	RequireAccept            []string          `json:"requireAccept"`
	RequireAdminConfirmation bool              `json:"requireAdminConfirmation"`
	Links                    map[string]string `json:"links"`
	RequireEmailConfirmation bool `json:"requireEmailConfirmation"`
}

type SchemaPrivate struct {
	SchemaPublic
	UserResource string                   `json:"userResource"`
	Apps         []App                    `json:"apps"`
	Tabs         []Tab                    `json:"tabs"`
	Resources    map[string][]FieldSchema `json:"resources"`
	Viewpoints   []string                 `json:"viewpoints"`
}
