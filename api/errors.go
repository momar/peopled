package api

import (
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"strconv"
	"strings"
)

var ErrRouteDoesntExist = errors.New("(404) route doesn't exist")
var ErrMethodNotAllowed = errors.New("(405) method not allowed")
var ErrMissingPermission = errors.New("(403) missing permission")
var ErrUsernameOrPasswordMissing = errors.New("(400) username or password missing")
var ErrUserDoesntExist = errors.New("(410) user doesn't exist")
var ErrUsernameAlreadyExists = errors.New("(409) an account with this username already exists")
var ErrEmailAlreadyExists = errors.New("(409) an account with this email address already exists")
var ErrIncorrectPassword = errors.New("(401) incorrect password")
var ErrAuthenticationRequired = errors.New("(401) authentication required")
var ErrSignupsAreDisabled = errors.New("(403) signups are disabled")
var ErrRecoveryIsDisabled = errors.New("(403) recovery is disabled")
var ErrResourceTypeDoesntExist = errors.New("(404) resource type doesn't exist")
var ErrResourceDoesntExist = errors.New("(404) resource doesn't exist")
var ErrResourceExists = errors.New("(409) resource exists")
var ErrUnparsableRequestBody = errors.New("(400) unparsable request body")
var ErrInvalidRecoveryToken = errors.New("(401) invalid recovery token")
var ErrInvalidConfirmationToken = errors.New("(401) invalid confirmation token")
var ErrCaptchaIsDisabled = errors.New("(410) captcha is disabled")

func returnError(c *gin.Context, err error) {
	msg := err.Error()
	if strings.HasPrefix(msg, "(") && strings.Index(msg, ") ") == 4 {
		if code, err := strconv.Atoi(msg[1:4]); err == nil {
			c.AbortWithStatusJSON(code, struct{ Error string `json:"error"` }{msg[6:]})
			return
		}
	}
	log.Warn().Err(err).Str("method", c.Request.Method).Str("path", c.Request.URL.Path).Msg("internal request error")
	c.AbortWithStatusJSON(500, struct{ Error string `json:"error"` }{"internal error"})
}

func returnErrors(c *gin.Context, errs map[string]error) {
	errStrings := map[string]string{}
	for k := range errs {
		if _, err := strconv.Atoi(k); err == nil {
			returnError(c, errs[k])
			return
		}
		errStrings[k] = errs[k].Error()
	}
	c.JSON(400, gin.H{"error": errStrings})
}
