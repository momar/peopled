package api

import (
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"github.com/ulule/limiter/v3"
	ginlimiter "github.com/ulule/limiter/v3/drivers/middleware/gin"
	"github.com/ulule/limiter/v3/drivers/store/memory"
	"os"
	"reflect"
	"strings"
	"time"
)

func Setup(prefix string, app *gin.Engine, noRoute ...gin.HandlerFunc) {
	prefix = strings.TrimSuffix(prefix, "/")

	failed := false
	for _, pluginConfig := range ActiveConfig.PluginConfig {
		err := pluginConfig.SetupPlugin()
		if err != nil {
			log.Error().Err(err).Msgf("failed to setup plugin config: %s", reflect.TypeOf(pluginConfig).String())
			failed = true
		} else {
			log.Debug().Msgf("plugin config has been set up: %s", reflect.TypeOf(pluginConfig).String())
		}
	}
	for resName, resType := range ActiveConfig.Resources {
		err := resType.Setup()
		if err != nil {
			log.Error().Err(err).Msgf("failed to setup resource type: %s", resName)
			failed = true
		} else {
			log.Debug().Msgf("resource type has been set up: %s", resName)
		}
	}
	if failed {
		os.Exit(1)
	}

	// rate limiter
	loginRate, _ := limiter.NewRateFromFormatted("30-H")
	loginLimiter := ginlimiter.NewMiddleware(limiter.New(memory.NewStore(), loginRate, limiter.WithTrustForwardHeader(EnvIsTrue("TRUST_PROXY", false))))
	captchaRate := limiter.Rate{"60-M", 10 * time.Second, 10}
	captchaLimiter := ginlimiter.NewMiddleware(limiter.New(memory.NewStore(), captchaRate, limiter.WithTrustForwardHeader(EnvIsTrue("TRUST_PROXY", false))))
	usernameRate, _ := limiter.NewRateFromFormatted("60-M")
	usernameLimiter := ginlimiter.NewMiddleware(limiter.New(memory.NewStore(), usernameRate, limiter.WithTrustForwardHeader(EnvIsTrue("TRUST_PROXY", false))))

	// Limit requests with invalid tokens
	requireToken := func(c *gin.Context) {
		if username, token := getToken(c, "login"); username == "" || token == "" {
			returnError(c, ErrAuthenticationRequired)
			return
		} else {
			c.Next()
		}
	}

	app.POST(prefix + "/authentication", loginLimiter, doCreateAuthentication)
	app.DELETE(prefix + "/authentication", requireToken, doDeleteAuthentication)
	app.GET(prefix + "/user", requireToken, doRetrieveCurrentUser)
	app.GET(prefix + "/user/:username", usernameLimiter, doCheckUsername)
	app.GET(prefix + "/signup", captchaLimiter, doGetCaptcha)
	app.POST(prefix + "/signup", loginLimiter, doCreateSignup)
	app.POST(prefix + "/signup/:token", loginLimiter, doConfirmSignup)
	app.POST(prefix + "/recovery", loginLimiter, doCreateRecovery)
	app.POST(prefix + "/recovery/:token", loginLimiter, doConfirmRecovery)
	app.GET(prefix + "/schema", loginLimiter, schema)

	app.GET(prefix + "/resource/:type", requireToken, resourceList)
	app.POST(prefix + "/resource/:type", requireToken, resourceCreate)
	app.GET(prefix + "/resource/:type/:id", requireToken, resourceGet)
	app.PUT(prefix + "/resource/:type/:id", requireToken, resourceUpdate)
	app.DELETE(prefix + "/resource/:type/:id", requireToken, resourceDelete)


	// return ErrMethodNotAllowed manually (no idea how Gin supports it in groups...)
	app.NoRoute(append([]gin.HandlerFunc{func(c *gin.Context) {
		if c.Request.URL.Path == prefix {
			c.Redirect(302, prefix + "/")
			return
		}
		if !strings.HasPrefix(c.Request.URL.Path, prefix + "/") {
			c.Next()
			return
		}
		p := strings.TrimPrefix(c.Request.URL.Path, prefix)
		i := strings.LastIndex(p, "/")
		if i < 0 {
			i = len(p)
		}
		p0 := p[:i]
		if p == "/authentication" || p == "/user" || p == "/signup" || p == "/recovery" || p == "/resource" || p == "/schema" ||
			p0 == "/user" || p0 == "/signup" || p0 == "/recovery" || p0 == "/resource" ||
			(strings.HasPrefix(p, "/resource/") && strings.Count(p, "/") == 3) {
			returnError(c, ErrMethodNotAllowed)
		} else {
			returnError(c, ErrRouteDoesntExist)
		}
		c.Abort()
	}}, noRoute...)...)

}
