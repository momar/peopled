package api

import (
	"crypto/tls"
	"github.com/jordan-wright/email"
	"github.com/rs/zerolog/log"
	"net/smtp"
	"os"
	"reflect"
	"regexp"
	"strings"
)

///////////////////////////
// General Configuration //
///////////////////////////

// ActiveConfig is the currently used configurations
var ActiveConfig Config

// TODO: document
type Config struct {
	CanonicalRootURL          string
	ListenAddress             string
	UserResource              string
	RequireEmailConfirmation  bool
	RequireAccept             []string
	RequireAdminConfirmation  bool
	AdminConfirmationGroups   []string
	AdminConfirmationEmails   []string
	AdminConfirmationLanguage string
	Links                     map[string]string
	EnableSignup              bool
	EnableSignupCaptcha       bool
	EnableRecovery            bool
	EmailConfig               EmailConfig
	PluginConfig              []PluginConfig
	Resources                 map[string]ResourceType
	Apps                      []App
	Tabs                      []Tab
}

type App struct {
	Name        TranslatedString `json:"name"`
	Description TranslatedString `json:"description"`
	URL         string           `json:"url"`
	Icon        string           `json:"icon"`
	Views       []string         `json:"views"`
}

type Tab struct {
	Name         TranslatedString       `json:"name"`
	Icon         string                 `json:"icon"`
	Type         string                 `json:"type"`
	ResourceType string                 `json:"resourceType"`
	ResourceID   string                 `json:"resourceID"`
	Views        []string               `json:"views"`
	Options      map[string]interface{} `json:"options"`
}

type TranslatedString map[string]string

func (t TranslatedString) T(lang string, value string) TranslatedString {
	x := TranslatedString{}
	for k, v := range t {
		x[k] = v
	}
	x[lang] = value
	return x
}

func T(lang string, value string) TranslatedString {
	return TranslatedString{lang: value, "": value}
}

// TODO: document
type PluginConfig interface {
	SetupPlugin() error
}

// GetPluginConfig retrieves the configuration for a specific plugin by the configuration's type.
func (config Config) GetPluginConfig(requestedType reflect.Type) interface{} {
	for _, pluginConfig := range config.PluginConfig {
		if pluginConfigType := reflect.TypeOf(pluginConfig); pluginConfigType != nil && pluginConfigType == requestedType {
			return pluginConfig
		}
	}
	return nil
}

////////////////////////////
// Resource Configuration //
////////////////////////////

// ResourceType represents a type of resources (e.g. users, groups, books, ...).
type ResourceType interface {
	Setup() error  // Validate the configuration of the resource type
	New() Resource // Create a new resource object
	ListCheck(viewpoint Viewpoint) error
	List(query Query) ([]string, error) // Get a list of resource IDs matching the given query
	Schema() []FieldSchema              // Get the schema for this resource type
}

// TODO: document
type AuthenticationResourceType interface {
	ResourceType
	Authenticate(username string, password string) (User, error) // must return the plain user object if the password is an empty string
	Signup(signup Signup, validateOnly bool) map[string]error
	SetPassword(username string, newPassword string) error
}

// Resource represents a specific resource (e.g. the user "admin") by its ID.
type Resource interface {
	ID() string

	Create(id string) error // TODO: document
	Read(id string) error   // Get the resource from the storage backend
	Update(id string) error // Store the resource & populate the ID

	Delete(id string, viewpoint Viewpoint) error // Delete the resource

	FromBody(body map[string]interface{}, viewpoint Viewpoint) map[string]error // Validate & apply the request body of a POST or PUT request
	ToBody(viewpoint Viewpoint) (map[string]interface{}, error)                 // Retrieve a response body for a GET request
}

// TODO: document
type FieldSchema struct {
	Name       string                 `json:"name"`    // The name of the field, as displayed in the frontend. TODO: translation?!
	Type       string                 `json:"type"`    // The type of the field, which has to be supported by the frontend.
	Options    map[string]interface{} `json:"options"` // The options of the field that are relevant for the presentation.

	ReadViews  []string               `json:"readViews"`
	WriteViews []string               `json:"writeViews"`
}

type Query map[string]QueryValue
type QueryValue struct {
	IsRegex bool
	IsEqual bool
	Value   string
	regex   *regexp.Regexp
}

var invalidRegexp = regexp.MustCompile(`$!`)

func (q QueryValue) Match(s string) bool {
	if q.IsEqual {
		return s == q.Value
	}
	if !q.IsRegex {
		return strings.Contains(s, q.Value)
	}
	if q.regex == nil {
		var err error
		q.regex, err = regexp.Compile(q.Value)
		if err != nil {
			q.regex = invalidRegexp
			return false
		}
	}
	return q.regex.MatchString(s)
}

/////////////////////////
// Email Configuration //
/////////////////////////

// EmailConfig for sending email via SMTP.
type EmailConfig struct {
	Server             string // The server's address including its port (e.g. "smtp.postmarkapp.com:587")
	Username           string // The username used to authenticate against the SMTP server
	Password           string // The password used to authenticate against the SMTP server
	UsePlainTLS        bool   // Force a plain TLS connection instead of opportunistic STARTTLS
	InsecureSkipVerify bool   // Accept any server certificate (insecure, only works with plain TLS)
	FromAddress        string // The sender address for emails
}

// Send an email according to the configuration.
func (emailConfig EmailConfig) Send(source *email.Email) error {
	source.From = emailConfig.FromAddress
	source.Sender = ""
	var auth smtp.Auth
	if emailConfig.Username != "" || emailConfig.Password != "" {
		auth = smtp.PlainAuth("", emailConfig.Username, emailConfig.Password, strings.TrimSuffix(strings.TrimRight(emailConfig.Server, "0123456789"), ":"))
	}
	if emailConfig.UsePlainTLS {
		return source.SendWithTLS(emailConfig.Server, auth, &tls.Config{InsecureSkipVerify: emailConfig.InsecureSkipVerify})
	} else {
		return source.Send(emailConfig.Server, auth)
	}
}

/////////////////////////
// Environment Helpers //
/////////////////////////

// EnvOrExit returns the value of an environment variable. If it's unset, the function prints an error message & exits.
func EnvOrExit(name string) string {
	if value, exists := os.LookupEnv(name); exists {
		return value
	} else {
		log.Error().Msgf("missing environment variable %s", name)
		os.Exit(1)
		return ""
	}
}

// EnvOrFallback returns the value of an environment variable. If it's unset, the function returns the fallback value.
func EnvOrFallback(name string, fallback string) string {
	if value, exists := os.LookupEnv(name); exists {
		return value
	} else {
		return fallback
	}
}

// EnvIsTrue returns the boolean value of an environment variable. If fallback is false, it only returns true for the
// values "1", "true", "yes" or "y". If fallback is true, it only returns false for the values "0", "false", "no" or "n".
func EnvIsTrue(name string, fallback bool) bool {
	if value, exists := os.LookupEnv(name); !exists {
		return fallback
	} else if fallback {
		value = strings.ToLower(value)
		return !(value == "0" || value == "false" || value == "no" || value == "n")
	} else {
		value = strings.ToLower(value)
		return value == "1" || value == "true" || value == "yes" || value == "y"
	}
}

// EnvSliceOrEmptySlice returns a slice from a comma-separated environment variable, or an empty slice if it doesn't exist.
func EnvSliceOrEmptySlice(name string) []string {
	slice := strings.Split(EnvOrFallback(name, ""), ",")
	for i := 0; i < len(slice); i++ {
		slice[i] = strings.TrimSpace(slice[i])
		if slice[i] == "" {
			slice = append(slice[:i], slice[i+1:]...)
			i--
		}
	}
	return slice
}
