package memory

import (
	"strings"
	"sync"
	"time"
)

type KV struct{
	store map[string]string
    timeouts map[string]int64
    lock sync.Mutex
	closed bool
}

func New() *KV {
	kv := &KV{
		map[string]string{},
		map[string]int64{},
		sync.Mutex{},
		false,
	}
	go func() {
		for !kv.closed {
			now := time.Now().Unix()
			for key, expiration := range kv.timeouts {
				if now > expiration {
					kv.lock.Lock()
					delete(kv.store, key)
					delete(kv.timeouts, key)
					kv.lock.Unlock()
				}
			}
			time.Sleep(30 * time.Minute)
		}
	}()
	return kv
}

func path(bucket string, key string) string {
	return strings.ReplaceAll(bucket, "+", "++") + "+" + strings.ReplaceAll(key, "+", "++")
}

func (kv *KV) Get(bucket string, key string) (string, time.Time) {
	p := path(bucket, key)
	kv.lock.Lock()
	var deadline time.Time
	expiration, ok := kv.timeouts[p]
	if ok && time.Now().Unix() > expiration {
		delete(kv.timeouts, p)
		delete(kv.store, p)
		return "", time.Time{}
	}
	if ok {
		deadline = time.Unix(expiration, 0)
	}
	value, _ := kv.store[p]
	kv.lock.Unlock()
	return value, deadline
}

func (kv *KV) Set(bucket string, key string, value string, timeout time.Duration) error {
	p := path(bucket, key)
	kv.lock.Lock()
	if timeout == 0 {
		if _, ok := kv.timeouts[p]; ok {
			delete(kv.timeouts, p)
		}
	} else {
		kv.timeouts[p] = time.Now().Add(timeout).Unix()
	}
	kv.store[p] = value
	kv.lock.Unlock()
	return nil
}

func (kv *KV) Del(bucket string, key string) error {
	p := path(bucket, key)
	if _, ok := kv.store[p]; ok {
		delete(kv.store, p)
		delete(kv.timeouts, p)
	}
	return nil
}

func (kv *KV) DelByValue(bucket string, value string) error {
	p := strings.ReplaceAll(bucket, "+", "++") + "+"
	for key, val := range kv.store {
		if strings.HasPrefix(key, p) && val == value {
			kv.lock.Lock()
			delete(kv.store, key)
			if _, ok := kv.timeouts[key]; ok {
				delete(kv.timeouts, key)
			}
			kv.lock.Unlock()
		}
	}
	return nil
}

func (kv *KV) Close() {
	kv.lock.Lock()
	kv.store = nil
	kv.timeouts = nil
	kv.closed = true
}
