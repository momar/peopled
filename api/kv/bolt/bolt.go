package bolt

import (
	"bytes"
	"strings"
	"time"
	bolt "go.etcd.io/bbolt"
)

type KV struct{
	db *bolt.DB
}

func New(p string) (*KV, error) {
	db, err := bolt.Open(p, 0600, nil)
	if err != nil {
		return nil, err
	}
	kv := &KV{db}
	go func() {
		for kv.db != nil {
			now := time.Now().Unix()
			kv.db.Update(func(tx *bolt.Tx) error {
				tx.ForEach(func(name []byte, b *bolt.Bucket) error {
					b.ForEach(func(k, v []byte) error {
						if strings.HasPrefix(string(k), "--timeout--") {
							if now > byte2time(v) {
								tx.Bucket(name).Delete(k)
								tx.Bucket(name).Delete([]byte(strings.TrimPrefix(string(k), "--timeout--")))
							}
						}
						return nil
					})
					return nil
				})
				return nil
			})
			time.Sleep(30 * time.Minute)
		}
	}()
	return kv, nil
}

func (kv *KV) Get(bucket string, key string) (string, time.Time) {
	var deadline time.Time
	var value string
	kv.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucket))
		if b == nil {
			return nil
		}
		timeout := b.Get([]byte("--timeout--" + key))
		if timeout != nil && time.Now().Unix() > byte2time(timeout) {
			b.Delete([]byte(key))
			b.Delete([]byte("--timeout--" + key))
			return nil
		}
		if timeout != nil {
			deadline = time.Unix(byte2time(timeout), 0)
		}
		valueByte := b.Get([]byte(key))
		if valueByte != nil {
			value = string(valueByte)
		}
		return nil
	})
	return value, deadline
}

func (kv *KV) Set(bucket string, key string, value string, timeout time.Duration) error {
	return kv.db.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte(bucket))
		if err != nil {
			return err
		}
		if timeout == 0 {
			err = b.Delete([]byte("--timeout--" + key))
			if err != nil {
				return err
			}
		} else {
			err = b.Put([]byte("--timeout--" + key), time2byte(time.Now().Add(timeout).Unix()))
			if err != nil {
				return err
			}
		}
		err = b.Put([]byte(key), []byte(value))
		return err
	})
}

func (kv *KV) Del(bucket string, key string) error {
	return kv.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucket))
		if b == nil {
			return nil
		}
		err := b.Delete([]byte("--timeout--" + key))
		if err != nil {
			return err
		}
		err = b.Delete([]byte(key))
		return err
	})
}

func (kv *KV) DelByValue(bucket string, value string) error {
	bucketBytes := []byte(bucket)
	valueBytes := []byte(value)
	return kv.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucket))
		if b == nil {
			return nil
		}
		b.ForEach(func(k, v []byte) error {
			if bytes.Equal(v, valueBytes) {
				tx.Bucket(bucketBytes).Delete(k)
				tx.Bucket(bucketBytes).Delete(append([]byte("--timeout--"), k...))
			}
			return nil
		})
		return nil
	})
}

func (kv *KV) Close() {
	kv.db.Close()
	kv.db = nil
}


func time2byte(v int64) []byte {
	b := []byte{0}
	if v < 0 {
		b[0] = 1
		v = -v
	}
	for v > 0 {
		b = append(b, byte(v & 0xff))
		v = v >> 8
	}
	return b
}
func byte2time(b []byte) int64 {
	var v int64
	if len(b) == 0 {
		return 0
	}
	for i := len(b)-1; i > 0; i-- {
		v = (v << 8) + int64(b[i])
	}
	if b[0] == 1 {
		return -v
	}
	return v
}