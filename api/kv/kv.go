package kv

import (
	"time"
)

type Store interface{
	Get(bucket string, key string) (string, time.Time)
	Set(bucket string, key string, value string, timeout time.Duration) error
	Del(bucket string, key string) error
	DelByValue(bucket string, value string) error
	Close()
}
